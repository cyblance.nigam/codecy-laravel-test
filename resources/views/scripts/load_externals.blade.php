
@include('scripts.googleanalytics')

@php 
    $locale = \App::getLocale();
@endphp 

@includeFirst([
    'scripts.civiccookie_'.$locale,
    'scripts.civiccookie_en',
])