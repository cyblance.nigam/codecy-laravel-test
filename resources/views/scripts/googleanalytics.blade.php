@if(false) 
{{-- google analytics is handled by civic cookie --}}
@if(config('eiie.google-analytics-tracking-id'))
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config('eiie.google-analytics-tracking-id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ config("eiie.google-analytics-tracking-id") }}');
    </script>
@endif
@endif 
