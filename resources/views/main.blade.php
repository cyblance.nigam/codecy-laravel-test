<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

@section('title', isset($title) ? $title : '')

@section('pageclass')
    @php 
        $route = Route::currentRouteName();
        if ($route) {
            echo 'route-'.str_replace('.','-', $route);
        }
    @endphp 
@endsection 

@include('shared.socials')

<head>
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
    <title>@yield('title')</title>
    <link rel="preload" href="https://use.typekit.net/scu0unl.css" as="style">

    <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/icons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="https://use.typekit.net/scu0unl.css">
    <link rel="stylesheet" type="text/css" href="{{ url('public/css/style.css') }}" />

    @yield('head')
	<!-- <script src="{{ mix('js/script.js') }}"></script> -->
    <script>var WS_PATH ='{{ url('/') }}';</script>
     <script src="{{ url('public/js/script.js')}}"></script>
</head>
<body class="@yield('pageclass')">
    <div class="wrapper">
        <header id="header_page">
            <h1><a href="{{ route('home') }}">Ei-iE</a></h1>
            <div id="navToggle">
            	<span>&nbsp;</span>
            	<span>&nbsp;</span>
            	<span>&nbsp;</span>
            </div>
            <nav id="mainNav">
                <div id="searchBar-mobile">@include('shared.searchBar', ['suffix' => '-mobile'])</div>
                <div id="languageSelector-mobile">@include('shared.language_selector', ['suffix' => '-mobile'])</div>
                <x-primary-navigation />
                <div id="memberNav-mobile" target="_blank"><a href="https://eiie.sharepoint.com/SitePages/HomePage.aspx">{{ $labelsArr['Members only'] }}</a></div>
            </nav>
            {{-- @yield('collections_nav') --}}
            <section id="headerside">
	            <div id="searchBar">@include('shared.searchBar')</div>
	            <div id="languageSelector">@include('shared.language_selector')</div>
                <div id="memberNav" target="_blank"><a href="https://eiie.sharepoint.com/SitePages/HomePage.aspx">{{ $labelsArr['Members only'] }}</a></div>
            </section>
        </header>
		<section id="main_page">
	        @yield('content')
		</section>
        <footer id="footer_page">
            <div class="social-profiles">
                <a href="https://www.facebook.com/educationinternational" target="_blank" rel="noopener" class="social-profile-facebook">Facebook</a>
                <a href="https://twitter.com/eduint" target="_blank" rel="noopener" class="social-profile-twitter">Twitter</a>
                <a href="https://www.youtube.com/user/EduInternational" target="_blank"  rel="noopener" class="social-profile-youtube">YouTube</a>
                <a href="https://soundcloud.com/user-918336677-743440864" target="_blank" rel="noopener" class="social-profile-soundcloud">Soundcloud</a>
            </div>
            <p>
                {!! $labelsArr['footer_licence'] !!}
            </p>
            <p>
                <a href="{{ route('contact.legal-notice') }}">{{ $labelsArr['Legal Notice'] }}</a>
                <a href="{{ route('contact.data-protection-policy') }}">{{ $labelsArr['Data Protection Policy'] }}</a>
            </p>
        </footer>
    </div>
    @include('scripts.load_externals')
</body>
</html>

