<section id="languageSelection{{ $suffix ?? '' }}">
<button id="languageSelection_toggle{{ $suffix ?? '' }}" aria-label="{{ $labelsArr['Show languages'] }}"></button>
<ul id="languageSelection_list{{ $suffix ?? '' }}">
@php

    
    
        $results = \App\Models\Language::select('name','alias_name')->where('is_active','active')->get();
    
@endphp
@foreach ($results as $localeKey => $locale)
    @if ($locale->alias_name != app()->getLocale())
        @php 
            $segs = Request::segments();
            $segs[0] = $locale->alias_name;
        @endphp 
        <li><a href="{{url('/')}}/{{ implode('/', $segs) }}">
            {{ $locale->name }}
        </a></li>
    @else 
        <li class="active"><span>{{ $locale->name }}</span></li>
    @endif 
@endforeach
</ul>
</section>
