@if (count($authors) > 0)
<span class="authors">
<span class="label">{{ $labelsArr['written by'] }}:</span>
@foreach ($authors as $author)
    <span class="author">
        <x-link :collection="$author" />
    </span>
@endforeach
</span>
@endif
