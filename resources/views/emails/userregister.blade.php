{{--  Mail to send user when register Added by cyblance --}}
<b>Hello!</b> {{ ucfirst($Userdetails['name']) }},<br><br>
Welcome to Education International. <br />
Thank you for your registration. You have registered as <b>{{ ucfirst($Userdetails['role']) }}</b><br>
Please https://dev.ei-ie.org/EiiE-website-2021-latest/login to login to your user account<br><br>
<b>Email</b>    :- {{ $Userdetails['email'] }}<br>
<b>Password</b> :- {{ $Userdetails['password']}}<br>
<br /><br />Best,<br />Education International.<br /><br />
