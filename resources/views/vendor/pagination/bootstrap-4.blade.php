@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item page-item-prev disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">
                        {{-- <span class="page-link-label-short">&lsaquo;</span> --}}
                        <span class="page-link-label-full">{!! $labelsArr['previous'] !!}</span>
                    </span>
                </li>
            @else
                <li class="page-item page-item-prev">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                        {{-- <span class="page-link-label-short">&lsaquo;</span> --}}
                        <span class="page-link-label-full">{!! $labelsArr['previous'] !!}</span>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-dots" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item page-item-next">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                        {{-- <span class="page-link-label-short">&rsaquo;</span> --}}
                        <span class="page-link-label-full">{!! $labelsArr['next'] !!}</span>
                    </a>
                </li>
            @else
                <li class="page-item page-item-next disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true">
                        {{-- <span class="page-link-label-short">&rsaquo;</span> --}}
                        <span class="page-link-label-full">{!! $labelsArr['next'] !!}</span>
                    </span>
                </li>
            @endif
        </ul>
    </nav>
@endif
