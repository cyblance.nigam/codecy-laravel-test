import React from "react";

import { GridColDef, GridValueFormatterParams } from "@material-ui/data-grid";
import dayjs from "dayjs";

import { Collection, Content, Item } from "../Models";

export const makeDateColumn = (field: string): GridColDef => ({
    field,
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams) =>
        dayjs(params.value as string).format("YYYY-MM-DD HH:mm"),
});

export const LangColumn: GridColDef = {
    field: "lang",
    sortable: false,
    renderCell: (params) => (
        <>
            {(params.row as Item | Collection).contents
                .map((content: Content) => content.lang)
                .join(",")}
        </>
    ),
};
