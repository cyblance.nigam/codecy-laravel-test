import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) => ({
    appBar: {
        top: 0,
        zIndex: theme.zIndex.drawer + 1,
    },
    appToolbar: {
        "& button": {
            marginLeft: theme.spacing(2),
        },
    },
    appBarTitle: {
        flexGrow: 1,
    },
    searchField: {
        backgroundColor: "rgba(255, 255, 255, 0.2)",
        maxWidth: 180,
        width: 300,
        transition: "all 0.4s ease-in-out",

        "&.Mui-focused": {
            backgroundColor: "rgba(255,255,255,0.8)",
            // minWidth: 300,
            maxWidth: 1000,
        },
    },
}));

export default useStyles;
