import React, { createContext, useContext, useState } from "react";

import StyledEngineProvider from "@material-ui/core/StyledEngineProvider";
import AdapterDayjs from "@material-ui/lab/AdapterDayjs";
import LocalizationProvider from "@material-ui/lab/LocalizationProvider";
import dayjs from "dayjs";
import utc from "dayjs/plugin/utc";

import Themed from "./Themed";

dayjs.extend(utc);

interface IAppContext {
    needSave: boolean;
    setNeedSave: (needSave: boolean) => void;
}

// interface IAppContextProvider {}
export const AppProvider: React.FC = ({ children }) => {
    const [needSave, setNeedSave] = useState(false);
    return (
        <AppContext.Provider value={{ needSave, setNeedSave }}>
            <Themed>
                <StyledEngineProvider injectFirst>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        {children}
                    </LocalizationProvider>
                </StyledEngineProvider>
            </Themed>
        </AppContext.Provider>
    );
};

export const AppContext = createContext<IAppContext | undefined>(undefined);
export const useAppContext = () => useContext(AppContext)!;

export default AppContext;
