import React from "react";

import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/styles";
import clsx from "clsx";

const useStyles = makeStyles(() => ({
    container: {
        paddingTop: 64,
        height: "100vh",
        boxSizing: "border-box",
        overflowY: "auto",
    },
}));
interface IProps {
    className?: string;
}
export const ContentScroll: React.FC<IProps> = ({ children, className }) => {
    const classes = useStyles();
    return <Box className={clsx(classes.container, className)}>{children}</Box>;
};

export default ContentScroll;
