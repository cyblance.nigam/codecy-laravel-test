import { Collection, Item } from "../Models";

export interface ItemResetAction {
    type: "item_reset";
    item: Item;
}
export interface CollectionResetAction {
    type: "collection_reset";
    collection: Collection;
}
export interface PatchAction {
    type: "patch";
    field: string;
    value: any;
}
export interface AddAttachmentGroupAction {
    type: "attachmentgroup_add";
}
export interface RemoveAttachmentGroupAction {
    type: "attachmentgroup_remove";
    index: number;
}
export interface UpdateAttachmentGroupAction {
    type: "attachmentgroup_update";
    index: number;
    field: string;
    value: any;
}
export interface AddAttachmentAction {
    type: "attachment_add";
    groupIndex?: number;
    attachedItem: Item;
}

export type ItemAction =
    | PatchAction
    | AddAttachmentGroupAction
    | RemoveAttachmentGroupAction
    | UpdateAttachmentGroupAction
    | AddAttachmentAction
    | ItemResetAction;

export type CollectionAction = CollectionResetAction | PatchAction;

// export type Action = ItemAction | CollectionAction;
