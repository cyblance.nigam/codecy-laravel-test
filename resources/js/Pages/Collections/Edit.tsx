import React, { useCallback, useEffect, useReducer, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { Box, Button, Divider, Grid, Typography } from "@material-ui/core";
import { Delta, formatters } from "jsondiffpatch";
import route from "ziggy-js";

import "jsondiffpatch/dist/formatters-styles/annotated.css";

import CollectionItemsImport from "../../Components/CollectionItemsImport";
import CollectionItemsOrdering from "../../Components/CollectionItemsOrdering";
import CollectionsManager from "../../Components/CollectionsManager";
import { CollectionGroups } from "../../Components/CollectionsManager.constants";
import ContentsEditor from "../../Components/ContentsEditor";
import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import ImagesManager from "../../Components/General/ImagesManager";
import { get as inertiaGet } from "../../Components/General/LinkQuery";
import MetaInfoManager from "../../Components/MetaInfoManager";
import Section, {
    CollectionContextSummary,
    SectionSummary,
} from "../../Components/Section";
// import SlotCard from "../../Components/SlotCard";
import Sorter, { CollectionLinkSorterRenderer } from "../../Components/Sorter";
import { AppBarHeader, ContentScroll, useAppContext } from "../../Layout";
import {
    Collection,
    findTitle,
    CollectionContent,
    ICollectionPageProps,
} from "../../Models";
import collectionReducer from "../../Stores/collectionReducer";
import DispatchProvider from "../../Stores/DispatchProvider";
import jsondiff from "../../Utils/jsondiff";
//Added By Cyblance for delete functionality
import DeleteIcon from "@material-ui/icons/Delete";


const Edit: React.FC<ICollectionPageProps> = ({ collection: _collection }) => {
    const [collection, dispatch] = useReducer(collectionReducer, _collection);
    const [diff, setDiff] = useState<Delta>();
    const { needSave, setNeedSave } = useAppContext();

    const onSave = () => {
        Inertia.patch(
            route("admin.collections.update", { collection }).toString(),
            diff,
            { preserveState: true }
        );
        setNeedSave(false);
    };

    const onReset = () => {
        // setCollection(_collection);
        dispatch({ type: "collection_reset", collection: _collection });
    };

    const onChangeContents = useCallback((contents: CollectionContent[]) => {
        dispatch({
            type: "patch",
            field: "contents",
            value: contents,
        });
        
    }, []);

    useEffect(() => {
        // console.log("changed item", item !== _item);
        setNeedSave(!!diff);
    }, [setNeedSave, diff]);

    useEffect(() => {
        // setCollection(_collection);
        dispatch({ type: "collection_reset", collection: _collection });
    }, [_collection]);

    useEffect(() => {
        setDiff(jsondiff.diff(_collection, collection));
    }, [_collection, collection]);

    // const onChangeSlot = (slot: Slot) => {
    //     setCollection((collection) => {
    //         if (!collection.slot_items?.length) {
    //             return {
    //                 ...collection,
    //                 slot_items: [{ slot_id: slot.id, item_id: slot.item_id }],
    //             };
    //         }
    //         const newSlotItems = collection.slot_items.map(
    //             ({ slot_id, item_id }) => {
    //                 if (slot.id === slot_id) {
    //                     return { slot_id, item_id: slot.item_id };
    //                 } else {
    //                     return { slot_id, item_id };
    //                 }
    //             }
    //         );
    //         return {
    //             ...collection,
    //             slot_items: newSlotItems,
    //         };
    //     });
    // };

    // useEffect(() => {
    //     if (!slots_template || !collection?.slot_items?.length) {
    //         return;
    //     }
    //     const slots: Slot[] = [...slots_template];
    //     collection.slot_items.forEach(({ item_id, slot_id }) => {
    //         const idx = slots.findIndex(({ id }) => id === slot_id);
    //         if (idx >= 0) {
    //             slots[idx] = { ...slots[idx], item_id };
    //         }
    //     });
    //     setSlots(slots);
    // }, [collection.slot_items, slots_template]);
    
    //Added By Cyblance for delete functionality
    const onCollectionDelete = () => {
        Inertia.post(route("admin.collections.delCollection", { collection }).toString(), {
        });
    };
    return (
        <DispatchProvider dispatch={dispatch}>
            <AppBarHeader title="Edit Collection">
                <Button
                    variant="outlined"
                    onClick={onReset}
                    color="secondary"
                    disabled={!needSave}
                >
                    Reset
                </Button>
                <Button
                    variant={needSave ? "contained" : "outlined"}
                    onClick={onSave}
                    color="secondary"
                    disabled={!needSave}
                >
                    Save
                </Button>
            </AppBarHeader>
            <ContentScroll>
                <form autoComplete="off" autoCapitalize="off">
                    <Box padding={2}>
                        <Typography variant="h4">
                            {findTitle(collection)}
                        </Typography>
                    </Box>
                    <MetaInfoManager<Collection>
                        item={collection}
                        // onChange={onChange}
                    />
                    <Section
                        title="Contents"
                        open={true}
                        summary={
                            <SectionSummary
                                contents={[
                                    {
                                        title: "Languages",
                                        text:
                                            collection.contents
                                                ?.map(({ lang }) => lang)
                                                .join(", ") || "No content",
                                    },
                                ]}
                            />
                        }
                    >
                        <ContentsEditor<CollectionContent>
                            contents={collection.contents}
                            fields={["title", "subtitle", "blurb"]}
                            htmlFields={{ blurb: "limited" }}
                            onChange={onChangeContents}
                            // (contents) =>
                            //     // onChange("contents", contents)
                            //     dispatch({
                            //         type: "patch",
                            //         field: "contents",
                            //         value: contents,
                            //     })
                            // }
                        />
                    </Section>

                    <ImagesManager
                        images={collection.all_images}
                        support_color={collection.support_color}
                        onSave={onSave}
                    />

                    {/* {slots.length ? (
                        <Section title="Slots">
                            <Grid container spacing={2}>
                                {slots.map((slot, i) => (
                                    <Grid item xs={2} key={i}>
                                        <SlotCard
                                            slot={slot}
                                            onChange={onChangeSlot}
                                        />
                                    </Grid>
                                ))}
                            </Grid>
                        </Section>
                    ) : undefined} */}

                    <Section
                        title="Context"
                        summary={
                            <CollectionContextSummary collection={collection} />
                        }
                    >
                        <Typography variant="h6">Items</Typography>
                        <Typography variant="body1">
                            This collection contains{" "}
                            {/*Update count of item after import By Cyblance*/}
                            <strong>{_collection.items_count || 0} items</strong>
                        </Typography>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                { /* save Content data on select or ad context addedd by Cyblance */ }
                                <ButtonConfirmDialog
                                    label="Create item"
                                    color="primary"
                                    needConfirmation={needSave}
                                    onConfirm={() => {
                                        inertiaGet("admin.items.create", {
                                            collection_id: collection.id,
                                        });
                                    }}
                                    variant="outlined"
                                > Before proceeding further to Edit, Please click on "SAVE"  and then "Confirm" to prevent loss of updated data.
                                <Button onClick={onSave}>Save</Button>
                                </ButtonConfirmDialog>
                                <br />
                                <Typography variant="caption">
                                    in this collection
                                </Typography>
                                {/* <ResourcePicker
                                    label="Add existing item"
                                    onPick={(item) => console.log(item)}
                                    menu={CollectionAddItemMenu}
                                    buttonProps={{
                                        variant: "outlined",
                                        size: "medium",
                                    }}
                                /> */}
                            </Grid>
                            <Grid item xs={4}>
                                <CollectionItemsImport /> <br />
                                <Typography variant="caption">
                                    from other collections
                                </Typography>
                            </Grid>
                            { /* save Content data on select or ad context addedd by Cyblance */ }
                            <Grid item xs={4}>
                                {collection.items_count ? (
                                    
                                    <ButtonConfirmDialog
                                        label="Show items"
                                        color="primary"
                                        needConfirmation={needSave}
                                        onConfirm={() => {
                                            inertiaGet("admin.items.index", {
                                                filter: {
                                                    ["collection.id"]:
                                                        collection.id,
                                                },
                                            });
                                        }}
                                        variant="outlined"
                                        > Before proceeding further to Edit, Please click on "SAVE"  and then "Confirm" to prevent loss of updated data.
                                            <Button onClick={onSave}>Save</Button>
                                    </ButtonConfirmDialog>
                                ) : (
                                    <></>
                                )}
                            </Grid>
                            {(collection.ordering === "manual" ||
                                collection.ordering === "partial_date") &&
                            collection.items_count ? (
                                <CollectionItemsOrdering />
                            ) : (
                                <></>
                            )}
                        </Grid>

                        <Box marginY={2}>
                            <Divider variant="middle" />
                        </Box>

                        <Typography variant="h6">Parent collections</Typography>
                        <Typography variant="body1">
                            Choose a parent collection to place this collection
                            in a context tree.
                        </Typography>
                        <CollectionsManager
                            collections={collection.parent_collections ?? []}
                            groupings={CollectionGroups}
                            // noOrdering={false}
                            onChange={(collections) =>
                              
                                // onChange("parent_collections", collections)
                                dispatch({
                                    type: "patch",
                                    field: "parent_collections",
                                    value: collections,
                                })
                               
                            }
                        />

                        <Box marginY={2}>
                            <Divider variant="middle" />
                        </Box>

                        <Typography variant="h6">Sub collections</Typography>
                        <Typography variant="body1">
                            The sub collections of this collection.{" "}
                            {collection.ordering !== "manual" &&
                                `Ordered automatically by ${collection.ordering}.`}
                        </Typography>
                        {collection.ordering === "manual" &&
                        collection.sub_collections ? (
                            <Sorter
                                items={collection.sub_collections}
                                renderer={CollectionLinkSorterRenderer}
                                onChange={(collections) =>
                                    // onChange("sub_collections", collections)
                                    dispatch({
                                        type: "patch",
                                        field: "sub_collections",
                                        value: collections,
                                    })
                                }
                                // className={classes.sorter}
                                variant="row"
                            />
                        ) : (
                            <Box
                                display="flex"
                                flexDirection="row"
                                flexWrap="wrap"
                            >
                                {collection.sub_collections?.map((coll, i) => (
                                    <CollectionLinkSorterRenderer
                                        key={i}
                                        item={coll}
                                        dragHandle={<></>}
                                        removeHandle={<></>}
                                    />
                                ))}
                            </Box>
                        )}
                        { /* save Content data on select or ad context addedd by Cyblance */ }
                        <ButtonConfirmDialog
                            label="Create sub collection"
                            color="primary"
                            needConfirmation={needSave}
                            onConfirm={() => {
                                inertiaGet("admin.collections.create", {
                                    parent_collection_id: collection.id,
                                });
                            }}
                            variant="outlined"
                            >
                            Before proceeding further to Edit, Please click on "SAVE"  and then "Confirm" to prevent loss of updated data.
                            <Button onClick={onSave}>Save</Button>
                        </ButtonConfirmDialog>   

                    </Section>
                    {/*Added By Cyblance for delete functionality*/}
                    <Section title="Actions">
                        <Grid container spacing={4} alignItems="baseline">
                            <Grid item xs={4}>
                                <ButtonConfirmDialog
                                    color="secondary"
                                    label="Delete Collection"
                                    variant="contained"
                                    onConfirm={() => onCollectionDelete()}
                                    icon={<DeleteIcon />}
                                >
                                    Deleting the Colletion is irreversible.
                                </ButtonConfirmDialog>
                            </Grid>
                        </Grid>
                    </Section>

                    {process.env.NODE_ENV === "development" && (
                        <div>
                            <div>
                                <pre>{JSON.stringify(diff, undefined, 4)}</pre>
                            </div>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: diff
                                        ? formatters.annotated.format(
                                              diff,
                                              _collection
                                          )
                                        : "<p>no diff</p>",
                                }}
                            ></div>
                            <div style={{ whiteSpace: "pre-wrap" }}>
                                {JSON.stringify(collection, undefined, 4)}
                            </div>
                        </div>
                    )}
                </form>
            </ContentScroll>
        </DispatchProvider>
    );
};

export default Edit;
