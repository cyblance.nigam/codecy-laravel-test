import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailIcon from "@material-ui/icons/Email";
import route from "ziggy-js";

import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import Section from "../../Components/Section";
import { AppBarHeader, ContentScroll, useAppContext } from "../../Layout";
import { AuthPageProps, ErrorPageProps, Language } from "../../Models";

interface IProps extends AuthPageProps {
    languageModel: Language;
    is_actives: Record<string, string>;
}
const Edit: React.FC<IProps & ErrorPageProps> = ({
    languageModel: language,
    errors,
    is_actives,
    can,
}) => {
    const { needSave, setNeedSave } = useAppContext();

    const [name, setName] = useState(language.name);
    const [alias_name, setAliaseName] = useState(language.alias_name);
    const [is_active, setIsActive] = useState(language.is_active);

    useEffect(() => {
        setName(language.name);
        setAliaseName(language.alias_name);
        setIsActive(language.is_active);
    }, [language]);

    useEffect(() => {
        setNeedSave(
            name !== language.name || alias_name !== language.alias_name || is_active !== language.is_active
        );
    }, [setNeedSave, language, name, alias_name, is_active]);

    const onReset = () => {
        setName(language.name);
        setAliaseName(language.alias_name);
        setIsActive(language.is_active);
    };

    const onSave = () => {
        Inertia.patch(
            route("admin.languages.update", { language }).toString(),
            { alias_name, name, is_active },
            {
                preserveState: true,
            }
        );
        setNeedSave(false);
    };    

    return (
        <>
            <AppBarHeader title="Edit Language">
                <Button
                    variant="outlined"
                    onClick={onReset}
                    color="secondary"
                    disabled={!needSave}
                >
                    Reset
                </Button>
                <Button
                    variant={needSave ? "contained" : "outlined"}
                    onClick={onSave}
                    color="secondary"
                    disabled={!needSave}
                >
                    Save
                </Button>
            </AppBarHeader>
            <ContentScroll>
                <Section title="Language details" open={true}>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                label="Name"
                                variant="outlined"
                                fullWidth
                                helperText={errors?.name}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                value={alias_name}
                                onChange={(e) => setAliaseName(e.target.value)}
                                label="Aliase name"
                                variant="outlined"
                                fullWidth
                                helperText={errors?.alias_name}
                                InputProps={{
                                    readOnly: true,
                                }}
                            />
                        </Grid>
                        {can?.is_active?.update && (
                            <Grid item xs={12}>
                                <FormControl variant="outlined" fullWidth>
                                    <InputLabel>is_active</InputLabel>
                                    <Select
                                        value={is_active}
                                        onChange={(e) =>
                                            setIsActive(e.target.value as any)
                                        }
                                        label="Type"
                                        fullWidth
                                    >
                                        {Object.entries(is_actives).map(
                                            ([is_active, label], i) => (
                                                <MenuItem value={is_active} key={i}>
                                                    {label}
                                                </MenuItem>
                                            )
                                        )}
                                    </Select>
                                    <FormHelperText>
                                        {errors?.is_active}
                                    </FormHelperText>
                                </FormControl>
                            </Grid>
                        )}
                    </Grid>
                </Section>
                
            </ContentScroll>
        </>
    );
};

export default Edit;
