import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import IconButton from "@material-ui/core/IconButton";
import { GridColDef } from "@material-ui/data-grid";
import AddIcon from "@material-ui/icons/Add";
import route from "ziggy-js";

import Listing from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { Language } from "../../Models";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Paginated from "../../Models/Paginated";
import { makeDateColumn } from "../../Utils/DataGridUtils";

// Lable Automation Code
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import LinearProgress, { LinearProgressProps } from '@material-ui/core/LinearProgress';
import LabelIcon from '@material-ui/icons/Label';
import Tooltip from '@material-ui/core/Tooltip';
import LabelOffIcon from '@material-ui/icons/LabelOff';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';

interface IProps extends IListingPageProps, AuthPageProps {
    languages: Paginated<Language>;
}
const List: React.FC<IProps> = ({ languages, filter: _filter, sort, can }) => {
    const [search, setSearch] = useState<string>();
    const [filter, setFilter] = useState(_filter);
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: languages,
        search,
        filter,
        sort,
    });
    // Label Automation Code Start
    const [open, setOpen] = React.useState(false);
    const [id, labelsetID] = React.useState('');
    const [labelPer, setLabelPer] = React.useState(0);
    const languageHandleOpen = (id:any,labelPerStatus:number) => {
        labelsetID(id);
        setLabelPer(labelPerStatus);
        setOpen(true);  
    };
    //added Api Call For Progress-bar by cyblance
    const [progress, setProgress] = React.useState(0);
    const apiCall=()=>{
        fetch(route("admin.label_check",{id}),
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            }).then((res) => res.json())
                .then((data) => {
                setProgress(data);  
            }
        );
    }; 
    
    const [value, setValue] = React.useState(0);
    const translateHandleClose =()=>{
       apiCall();
        const apiCallTimmer = setInterval(() => {
            apiCall();
        },100);
        Inertia.post(route("admin.all-label-translate",{id}).toString(), {  }, {
            preserveState: true,
            onSuccess(info){ 
                clearInterval(apiCallTimmer);    
                setOpen(false); 
                setState({ opens: true, vertical: 'top', horizontal: 'center' });      
            }
        });
    };
    const languageHandleClose = () => {
        setOpen(false);
    };
    const [state, setState] = React.useState({
        opens: false,
        vertical: 'top',
        horizontal: 'center',
      });
    
    const { vertical, horizontal, opens } = state;
    const handleCloses = () => {
        setState({ ...state, opens: false });
    };
    const useStyles = makeStyles(() => ({
        root: {
            width: '100%',
        },
    }));
    //Added Progress Bar by cyblance
    const classes = useStyles();
    function LinearProgressWithLabel(props: LinearProgressProps & { value: number }) {
        return (
          <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
              <LinearProgress variant="determinate" {...props} />
            </Box>
            <Box minWidth={35}>
              <Typography variant="body2" color="textSecondary">{`${Math.round(
                props.value,
              )}%`}</Typography>
            </Box>
          </Box>
        );
    }
    // Label Automation Code End 
    const columns: GridColDef[] = [
        { field: "id" },
        {
            field: "name",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.languages.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as Language).name ?? "- no name -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        
        {
            field: "alias_name",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.languages.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as Language).alias_name ?? "- no alias name -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        {   //Added sortable as false by Cyblance
            field: "Label Translate",
            sortable: false,
            renderCell: (cell) => (
                <>
                <div>
                    <Grid container spacing={0}>
                        <Grid item xs={6}>
                            {cell.row.is_active=='in-active'?
                                <Tooltip title="Please Active languge First">
                                    <IconButton disabled>
                                        <LabelOffIcon />
                                    </IconButton>
                                </Tooltip>
                            :
                            <Tooltip title="Translate All Label">
                                <div>
                                    <IconButton  onClick={() => languageHandleOpen(cell.row.id,cell.row.label_translate_per)}>
                                        <LabelIcon /> 
                                    </IconButton> 
                                    {cell.row.label_translate_per=='100'?cell.row.label_translate_per+'% Completed' : cell.row.label_translate_per+"%"}
                               </div>
                            </Tooltip>
                            }
                        </Grid>
                    </Grid>
                </div>
                </>
            ),
            flex: 3,
        },
        { field: "is_active" },
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
    ];

    const onCreate = () => {
        Inertia.get(route("admin.languages.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const title = "Languages";

    return (
        <>
            <AppBarHeader
                title={title}
                filterOptions={[]}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton onClick={onCreate} disabled={!can?.languages?.create}>
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                <Listing columns={columns} dataSource={dataSource}></Listing>
            </ContentScroll>
            <Dialog open={open} onClose={languageHandleClose} aria-labelledby="form-dialog-title" maxWidth='sm' fullWidth={true}>
                <DialogTitle id="form-dialog-title">Translator</DialogTitle>
                <DialogContent>

                    <DialogContentText>
                    All Label Translator
                        {
                            progress==0 ?
                            <div className={classes.root}>
                            {labelPer==100 ? <LinearProgressWithLabel value={100} /> :  
                            <LinearProgressWithLabel value={labelPer} />}
                            </div>
                            :
                            <div className={classes.root}>
                            {labelPer==100 ? <LinearProgressWithLabel value={100} /> :  
                            <LinearProgressWithLabel value={progress} />}
                            </div>
                        }
                    </DialogContentText>
                    <FormControl style={{width:'100%'}}>
                        {labelPer==100 ?  <Button variant="contained" disabled onClick={translateHandleClose} color="primary">
                            Translate
                        </Button> : <Button variant="contained"  onClick={translateHandleClose} color="primary">
                            Translate
                        </Button>}
                    </FormControl>
                
                </DialogContent>
                <DialogActions>
                {progress==100 ?
                <Button onClick={languageHandleClose} variant="contained" color="primary">
                    Done
                </Button>  
                :""} 
                <Button onClick={languageHandleClose} color="secondary">
                    Cancel
                </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default List;
