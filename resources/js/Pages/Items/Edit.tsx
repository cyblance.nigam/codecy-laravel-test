import React, { useCallback, useEffect, useReducer, useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { Box, Button, Typography, Grid } from "@material-ui/core";
import { Delta, formatters } from "jsondiffpatch";
import route from "ziggy-js";
import "jsondiffpatch/dist/formatters-styles/annotated.css";
import AttachmentsManager from "../../Components/AttachmentsManager";
import { AuditSection } from "../../Components/Audit";
import CollectionsManager from "../../Components/CollectionsManager";
import { CollectionGroups } from "../../Components/CollectionsManager.constants";
import ContentsEditor from "../../Components/ContentsEditor";
import ImagesManager from "../../Components/General/ImagesManager";
import ImageResourceContext from "../../Components/ImageResourceContext";
import MetaInfoManager from "../../Components/MetaInfoManager";
import makeResourceEditor from "../../Components/Resources/makeResourceEditor";
import Section, {
    ItemContextSummary,
    SectionSummary,
} from "../../Components/Section";
import AppBarHeader from "../../Layout/AppBarHeader";
import { useAppContext } from "../../Layout/AppContext";
import ContentScroll from "../../Layout/ContentScroll";
import { isImageResource, ItemContent } from "../../Models";
import { findTitle } from "../../Models/Content";
import Item from "../../Models/Item"; 
import DispatchProvider from "../../Stores/DispatchProvider";
import itemReducer from "../../Stores/itemReducer";
import jsondiff from "../../Utils/jsondiff";
import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import DeleteIcon from "@material-ui/icons/Delete";

interface IProps {
    item: Item;
    layout: any;
}

const Edit: React.FC<IProps> = ({ item: _item }) => {
    const [item, dispatch] = useReducer(itemReducer, _item);
    const [diff, setDiff] = useState<Delta>();
    const { needSave, setNeedSave } = useAppContext();
   
    const onSave = () => {
        
        Inertia.patch(route("admin.items.update", { item }).toString(), diff, {
            preserveState: false,
        });
        setNeedSave(false);
    };
    const onReset = () => {
        dispatch({ type: "item_reset", item: _item });
    };

    const onChangeContents = useCallback((contents: ItemContent[]) => {
        dispatch({
            type: "patch",
            field: "contents",
            value: contents,
        });
    }, []);

    useEffect(() => {
        setNeedSave(!!diff);
    }, [diff, setNeedSave]);

    useEffect(() => {
        dispatch({ type: "item_reset", item: _item });
    }, [_item]);

    useEffect(() => {
        setDiff(jsondiff.diff(_item, item));
    }, [_item, item]);

    //Added By Cyblance for delete functionality
    const onItemDelete = () => {
        Inertia.post(route("admin.items.delItem", { item }).toString(), {
        });
    };

    return (
        <DispatchProvider dispatch={dispatch}>
            <AppBarHeader title="Edit Item">
                <Button
                    variant="outlined"
                    onClick={onReset}
                    color="secondary"
                    disabled={!needSave}
                >
                    Reset
                </Button>
                <Button
                    variant={needSave ? "contained" : "outlined"}
                    onClick={onSave}
                    color="secondary"
                    disabled={!needSave}
                >
                    Save
                </Button>
            </AppBarHeader>
            <ContentScroll>
                <form autoComplete="off" autoCapitalize="off">
                    {/* <Box
                        className={classes.container}
                        container
                        spacing={2}
                        alignItems="flex-end"
                    > */}
                    <Box padding={2}>
                        <Typography variant="h4">
                            {findTitle(item) ||
                                item.contents[0].title ||
                                "-no title-"}
                        </Typography>
                    </Box>
                    <MetaInfoManager<Item> item={item} />
                    <Section
                        title="Contents"
                        open={true}
                        summary={
                            <SectionSummary
                                contents={[
                                    {
                                        title: "Languages",
                                        text:
                                            item.contents
                                                ?.map(({ lang }) => lang)
                                                .join(", ") || "No content",
                                    },
                                ]}
                            />
                        }
                    >
                        <ContentsEditor<ItemContent>
                            contents={item.contents}
                            fields={
                                item.type === "contact"
                                    ? ["title", "subtitle", "blurb"]
                                    : ["title", "subtitle", "blurb", "content"]
                            }
                            htmlFields={{ content: "full", blurb: "limited" }}
                            itemSubtype={item.subtype}
                            additionalEditor={makeResourceEditor(item)}
                            onChange={onChangeContents}
                            onvalid={needSave}
                            // (contents) =>
                            //     // onChange("contents", contents)
                            //     dispatch({
                            //         type: "patch",
                            //         field: "contents",
                            //         value: contents,
                            //     })
                            // }
                        />
                    </Section>
                    <Section
                        title="Context"
                        summary={<ItemContextSummary item={item} />}
                    >
                        {isImageResource(item) && (
                            <ImageResourceContext item={item} />
                        )}
                        <CollectionsManager
                            collections={item.collections ?? []}
                            groupings={CollectionGroups}
                            onChange={(collections) =>
                                dispatch({
                                    type: "patch",
                                    field: "collections",
                                    value: collections,
                                })
                            }
                        />
                    </Section>
                    {!isImageResource(item) && (
                       
                        <ImagesManager
                            images={item.all_images}
                            support_color={item.support_color}
                            onSave={onSave}
                            azure={item.contents[0].azure}
                            // onChange={(images: Item[]) =>
                            //     onChange("all_images", images)
                            // }
                        />
                    )}
                    {!isImageResource(item) && item.subtype !== "file" && (
                        <AttachmentsManager
                            attachmentGroups={item.attachment_groups}
                            onSave={onSave}
                            // onChange={(attachmentGroups) =>
                            //     onChange("attachment_groups", attachmentGroups)
                            // }
                        />
                    )}
                    <AuditSection />
                    {/*Added By Cyblance for delete functionality*/}
                   <Section title="Actions">
                       <Grid container spacing={4} alignItems="baseline">
                            <Grid item xs={4}>
                                <ButtonConfirmDialog
                                    color="secondary"
                                    label="Delete Item"
                                    variant="contained"
                                    onConfirm={() => onItemDelete()}
                                    icon={<DeleteIcon />}
                                >
                                    Deleting the Item is irreversible.
                                </ButtonConfirmDialog>
                            </Grid>
                    </Grid> 
                    </Section>    
                    {process.env.NODE_ENV === "development" && (
                        <div>
                            <div>
                                <pre>{JSON.stringify(diff, undefined, 4)}</pre>
                            </div>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: diff
                                        ? formatters.annotated.format(
                                              diff,
                                              _item
                                          )
                                        : "<p>no diff</p>",
                                }}
                            ></div>
                            <div style={{ whiteSpace: "pre-wrap" }}>
                                {JSON.stringify(item, undefined, 4)}
                            </div>
                        </div>
                    )}
                </form>
            </ContentScroll>
        </DispatchProvider>
    );
};

export default Edit;
