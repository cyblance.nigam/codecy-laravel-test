import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import IconButton from "@material-ui/core/IconButton";
import { GridColDef } from "@material-ui/data-grid";
import AddIcon from "@material-ui/icons/Add";
import route from "ziggy-js";

import Listing from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { Label } from "../../Models";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Paginated from "../../Models/Paginated";
import { makeDateColumn } from "../../Utils/DataGridUtils";
import LabelIcon from '@material-ui/icons/Label';

interface IProps extends IListingPageProps, AuthPageProps {
    labels: Paginated<Label>;
}
const List: React.FC<IProps> = ({ labels, filter: _filter, sort, canLang }) => {
    const [search, setSearch] = useState<string>();
    const [filter, setFilter] = useState(_filter);
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: labels,
        search,
        filter,
        sort,
    });

    const columns: GridColDef[] = [
        { field: "id" },
        {
            field: "name",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.labels.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as Label).name ?? "- no name -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        { field: "is_active" },
        {
            field: "Edit Label Translate",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.labelTranslates.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    <IconButton>
                        <LabelIcon />
                    </IconButton>
                </InertiaLink>
            ),
            flex: 2,
        },
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
    ];

    const onCreate = () => {
        Inertia.get(route("admin.labels.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const title = "Labels";

    return (
        <>
            <AppBarHeader
                title={title}
                filterOptions={[]}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton onClick={onCreate} disabled={!canLang?.labels?.create}>
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                <Listing columns={columns} dataSource={dataSource}></Listing>
            </ContentScroll>
        </>
    );
};

export default List;
