import React, { useEffect, useState } from "react";

import { Inertia } from "@inertiajs/inertia";
import {
    Box,
    Button,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailIcon from "@material-ui/icons/Email";
import route from "ziggy-js";

import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import Section from "../../Components/Section";
import { AppBarHeader, ContentScroll, useAppContext } from "../../Layout";
import { AuthPageProps, ErrorPageProps, Language, LabelTranslate } from "../../Models";

interface IProps extends AuthPageProps {
    languageModel: Language;
    labelTranslateModel: LabelTranslate;
    //is_actives123: Record<string, string>;
    is_actives123: Record<string, string>;
}
const Edit: React.FC<IProps & ErrorPageProps> = ({
        labelTranslateModel: labelTranslate,
        errors,
        is_actives123,
        can,
        canLabelTranslate,
}) => {
    const { needSave, setNeedSave } = useAppContext();
    
    const [name, setName] = useState(is_actives123);

    
    function setLabelTranslate(key:any,value:any){
        
        is_actives123[key]= value;
        setName(is_actives123[key]= value);
        setNeedSave(true);
    }

    useEffect(()=>{
        setName(is_actives123)
    },[name]);

    const onSave = () => {
        let a = JSON.stringify(is_actives123);
        console.log(a);
        
        Inertia.patch(
            route("admin.labelTranslates.update", { labelTranslate }).toString(),
            {is_actives123} ,
            
        );
        setNeedSave(false);
    };

    const onReset = () => {
        setName(name);
        
    };    

    return (
        <>
            <AppBarHeader title="Edit label">
                <Button
                    variant="outlined"
                    onClick={onReset}
                    color="secondary"
                    disabled={!needSave}
                >
                    Reset
                </Button>
                <Button
                    variant={needSave ? "contained" : "outlined"}
                    onClick={onSave}
                    color="secondary"
                    disabled={!needSave}
                >
                    Save
                </Button>
            </AppBarHeader>
            <ContentScroll>
                <Section title="Label details" open={true}>
                    <Grid container>
                        <Grid item xs={12}>
                            {Object.entries(name).map(
                                ([is_active, label123], i) => (                                    

                                    <TextField
                                    value={label123}
                                    onChange={(e) => setLabelTranslate(is_active, e.target.value)}
                                    label={is_active}
                                    variant="outlined"
                                    fullWidth
                                    name={is_active}
                                    />
                                    
                                )
                            )}
                        </Grid>
                    </Grid>
                </Section>
            </ContentScroll>
        </>
    );
};

export default Edit;
