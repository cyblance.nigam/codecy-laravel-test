import React, { useEffect, useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import IconButton from "@material-ui/core/IconButton";
import { GridColDef } from "@material-ui/data-grid";
import AddIcon from "@material-ui/icons/Add";
import route from "ziggy-js";
import Listing from "../../Components/Listing";
import useDataSource from "../../Components/useDataSource";
import AppBarHeader from "../../Layout/AppBarHeader";
import ContentScroll from "../../Layout/ContentScroll";
import { User } from "../../Models";
import { AuthPageProps, IListingPageProps } from "../../Models/Inertia";
import Paginated from "../../Models/Paginated";
import { makeDateColumn } from "../../Utils/DataGridUtils";
import ButtonConfirmDialog from "../../Components/General/ButtonConfirmDialog";
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

//Added By Cyblance for delete functionality
import Box from '@material-ui/core/Box';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


interface IProps extends IListingPageProps, AuthPageProps {
    users: Paginated<User>;
}
const List: React.FC<IProps> = ({ users, filter: _filter, sort, can }) => {
    const [search, setSearch] = useState<string>();
    const [filter, setFilter] = useState(_filter);
    const dataSource = useDataSource({
        mode: "inertia",
        paginatedData: users,
        search,
        filter,
        sort,
    });

    //Added By Cyblance for delete functionality
    const handleChangeDel = () => {
        const getSessionData: any = sessionStorage.getItem("getId");
        const getData = JSON.parse(getSessionData);
        const count = getData.length;
        if (count == 0) {
            setState({ opens: true, vertical: 'top', horizontal: 'center' });
        } else {
            const getResult = getData.id;
            const getResultAction = actionTrash;
            Inertia.post(route("user.bulkDelete", { getResult, getResultAction }).toString(), {
            });
        }
    };
    const [state, setState] = React.useState({
        opens: false,
        vertical: 'top',
        horizontal: 'center',
    });
    const { vertical, horizontal, opens } = state;
    const handleCloses = () => {
        setState({ ...state, opens: false });
    };
    const [value, setValue] = React.useState(0);
    const handleChange = (newValue: any) => {
        setValue(newValue);
    };
    const [withoutTrash, setUnTrash] = React.useState('');
    const [withTrash, setTrash] = React.useState('');
    const [actionTrash, setActionTrash] = React.useState('');

    const handleTrash = (e: any) => {
        setActionTrash(e.target.value);
    }
    useEffect(() => {
        let currentUrl = window.location.href;
        let splitCurrentUrl = currentUrl.split('?');
        if (splitCurrentUrl[1] == 'trash=yes') {
                setValue(1);
        } else {
                setValue(0);
        }
        setUnTrash(splitCurrentUrl[0] + '?trash=no');
        setTrash(splitCurrentUrl[0] + '?trash=yes');
    });

    const columns: GridColDef[] = [
        { field: "id" },
        {
            field: "name",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.users.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as User).name ?? "- no name -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        {
            field: "email",
            renderCell: (cell) => (
                <InertiaLink
                    href={route("admin.users.edit", {
                        id: cell.row.id,
                    }).toString()}
                >
                    {(cell.row as User).email ?? "- no email -"}
                </InertiaLink>
            ),
            flex: 2,
        },
        { field: "role" },
        makeDateColumn("created_at"),
        makeDateColumn("updated_at"),
    ];

    const onCreate = () => {
        Inertia.get(route("admin.users.create"));
    };

    useEffect(() => {
        setFilter(_filter);
    }, [_filter]);

    const title = "Users";

    return (
        <>
            <AppBarHeader
                title={title}
                filterOptions={[]}
                filter={filter}
                onSearch={setSearch}
                onChangeFilter={setFilter}
            >
                <IconButton onClick={onCreate} disabled={!can?.users?.create}>
                    <AddIcon />
                </IconButton>
            </AppBarHeader>
            <ContentScroll>
                {/* Added By Cyblance for delete functionality */}
                <div>
                    <Box component="span" sx={{ p: 2 }}>
                    {
                        value == 0 ?
                        <FormControl variant="outlined" style={{width:'20%'}}>
                            <InputLabel id="demo-simple-select-outlined-label">Bulk Action</InputLabel>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                onChange={handleTrash}
                                label="Bulk Action"
                            >
                            <MenuItem value="trashed">Move to Trash</MenuItem>
                            </Select>
                        </FormControl>:
                        <FormControl variant="outlined" style={{width:'20%'}}>
                            <InputLabel id="demo-simple-select-outlined-label">Bulk Action</InputLabel>
                            <Select
                                labelId="demo-simple-select-outlined-label"
                                id="demo-simple-select-outlined"
                                onChange={handleTrash}
                                label="Bulk Action"
                                >
                                <MenuItem value="restored">Restore</MenuItem>
                                <MenuItem value="deleted">Delete permanently</MenuItem>
                            </Select>
                        </FormControl>
                    }
                    </Box>
                    <FormControl variant="outlined" style={{width:'10%'}}>
                        <ButtonConfirmDialog
                            color="primary"
                            label="Apply"
                            variant="contained"
                            onConfirm={() => handleChangeDel()}
                        >
                            Deleting the User is irreversible.
                        </ButtonConfirmDialog>
                    </FormControl>
                </div>
                
                <Snackbar
                    color="primary"
                    anchorOrigin={{ vertical: "top", horizontal: "center" }}
                    open={opens}
                    autoHideDuration={6000}

                    message="Please Select The User First"
                    key={vertical + horizontal}
                    action={
                        <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloses}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                        </React.Fragment>
                    }
                />
                
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={handleChange}
                    aria-label="disabled tabs example"
                >
                    <InertiaLink
                    href={withoutTrash}>
                        <Tab label="All" />
                    </InertiaLink>
                    <InertiaLink
                    href={withTrash}>
                        <Tab label="Trash" />
                    </InertiaLink>
                </Tabs>
                
                <Listing columns={columns} dataSource={dataSource}></Listing>
            </ContentScroll>
        </>
    );
};

export default List;
