import { ItemSubtype, ItemType, StatusType } from "../Config";

import { Affiliate } from "./Affiliate";
import { AttachmentGroup } from "./Attachment";
import Collection from "./Collection";
import { Contact } from "./Contact";
import Content from "./Content";
import { DCProject } from "./DCProject";
import {
    FileResource,
    ImageResource,
    LinkResource,
    VideoResource,
} from "./Resource";

export interface ItemContent extends Content {
    content?: string;
    content_json?: string;
    azure:string;
    images: ImageResource[];
    videos: VideoResource[];
    links: LinkResource[];
    files: FileResource[];
    contacts: Contact[];
    dcprojects: DCProject[];
}

export interface Item {
    id: number;
    type: ItemType;
    subtype: ItemSubtype;

    status: StatusType;

    created_at: string;
    updated_at: string;
    publish_at: string;

    content?: ItemContent;
    contents: ItemContent[];

    all_images: Item[];
    collections?: Collection[];
    attachment_groups: AttachmentGroup[];
    image_for_items_count?: number;
    image_for_collections_count?: number;
    affiliate?: Affiliate;

    old_id: string;
    old_type: string;

    support_color?: string;
}

export default Item;

export function isItemContent(content: Content): content is ItemContent {
    return (content as ItemContent).images !== undefined;
}
export function isItem(item: Item | Collection): item is Item {
    return (item as Item).subtype !== undefined;
}
