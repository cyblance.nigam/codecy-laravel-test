import $ from "jquery";

/* eslint-disable */

function isMobileMenu() {
    return $("#navToggle:visible").length > 0;
}

function layoutMenu() {
    if (isMobileMenu()) {
        $("#mainNav .nav_drop_listing").slideUp(0);
        $("#mainNav .nav_drop").slideDown(0);
    } else {
        $("#mainNav .nav_drop_listing").slideDown(0);
        if ($("#mainNav").css("display") != "none") {
            $("#mainNav").css("display", "");
        }
    }
}

$(document).ready(function () {
    $("#navToggle").click(function () {
        var navShow = $("#mainNav").hasClass("show");
        $("#mainNav").toggleClass("show", !navShow);
        $(document.body).toggleClass("active_nav", !navShow);
    });

    $("div.searchToggle").mouseenter(function () {
        $("#searchBar").addClass("show");
        $("#search_input").focus();
    });
    $("div.searchToggle").click(function () {
        // $("input#search_input").toggle();
        $("#searchBar").addClass("show");
        $("#search_input").focus();
    });
    $("#search_cancel_button").click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $("#searchBar").removeClass("show");
        return true;
    });

    $("#languageSelection_toggle").click(function () {
        // $("#languageSelection_list").toggle();
        $("#languageSelection_list").toggleClass("show");
    });

    $("#mainNav .nav_drop_section h2").click(function () {
        // if (isMobileMenu()) {
        // $(this).next(".nav_drop_listing").slideToggle();
        // }
        var dropDownIsActive = $(this).hasClass("active");
        $(this).parents(".nav_main").find("h2").removeClass("active");
        $(this)
            .parents(".nav_main")
            .find(".nav_drop_listing")
            .removeClass("show_nav_drop_listing");
        $(this)
            .toggleClass("active", !dropDownIsActive)
            .next(".nav_drop_listing")
            .toggleClass("show_nav_drop_listing", !dropDownIsActive);
    });

    $(".nav_main_item").hover(
        function () {
            $(".nav_drop", this).addClass("show_nav_drop");
            // if (!isMobileMenu()) {
            //     layoutMenu();
            //     $(".nav_drop", this).stop();
            //     $(".nav_drop", this).slideDown();
            // }
        },
        function () {
            $(".nav_drop", this).removeClass("show_nav_drop");
            // if (!isMobileMenu()) {
            //     $(".nav_drop", this).stop();
            //     $(".nav_drop", this).slideUp();
            // }
        }
    );

    $(".carousel").each(function () {
        var status = "stop";
        var carousel = this;
        var timer = false;
        var current = 0;
        var $items = $(".carousel_items article", this);
        var $controls = $(".carousel_controls span", this);

        function showCurrent() {
            // console.log("carousel status", status, current);
            $items.removeClass("carousel_show");
            $($items[current]).addClass("carousel_show");

            $controls.removeClass("carousel_active");
            $($controls[current]).addClass("carousel_active");
        }

        function pause() {
            if (status === "stop") {
                clearTimeout(timer);
                return; // stay stopped
            }
            status = "pause";
            clearTimeout(timer);
        }
        function resume() {
            status = "play";
            showCurrent();

            timer = setTimeout(function () {
                current = (current + 1) % $items.length;
                resume();
            }, 8000);
        }

        function stop() {
            status = "stop";
            clearTimeout(timer);
        }

        $(carousel).mouseenter(function () {
            if (status === "play") {
                pause();
            }
        });

        $(carousel).mouseleave(function () {
            if (status === "pause") {
                resume();
            }
        });

        $(".carousel_blocker", carousel).click(function (e) {
            $(this).css("pointer-events", "none");
            // console.log("click");
            stop();
            // $($items[0]).click(e);
        });

        $controls.click(function () {
            stop();
            current = parseInt($(this).attr("data-carousel-control-idx"));
            showCurrent();
        });

        resume();
    });

    $(".collection_items_inline .inline_item_do_show_content").click(
        function () {
            $(this)
                .parents("article.inline_item")
                .removeClass("inline_item_hide_content");
        }
    );

    $(".collection_items_inline .inline_item_do_hide_content").click(
        function () {
            $(this)
                .parents("article.inline_item")
                .addClass("inline_item_hide_content");
        }
    );

    $(".subcollections_list_unhide_all_siblings").click(function () {
        $(this)
            .parent("li")
            .addClass("subcollections_list_hide")
            .siblings()
            .removeClass("subcollections_list_hide");
    });

    // $(window).resize(function () {
    //     // layoutMenu();
    // });
});
