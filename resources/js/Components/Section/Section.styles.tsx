import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    section: {
        padding: theme.spacing(2),
        margin: theme.spacing(2),
        width: `calc(100% - ${theme.spacing(4)}px)`,
    },
    header: {
        margin: theme.spacing(-2),
        padding: theme.spacing(2),
        cursor: "pointer",
    },
    headerOpen: {
        paddingBottom: theme.spacing(1),
        marginBottom: theme.spacing(1),
        backgroundColor: theme.palette.background.default,
    },
}));

export default useStyles;
