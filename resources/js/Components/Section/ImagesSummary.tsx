import React, { ReactNode } from "react";

import { pascalCase } from "pascal-case";

import Item from "../../Models/Item";
import ImageCard from "../General/ImageCard";

import useStyles from "./ImagesSummary.styles";
import SectionSummary, { SectionSummaryContent } from "./SectionSummary";

interface IProps {
    images: Item[];
    addImageButton?: ReactNode;
    support_color?: string;
}
export const ImagesSummary: React.FC<IProps> = ({
    images,
    addImageButton,
    support_color,
}) => {
    const classes = useStyles();
    let contents: SectionSummaryContent[] = [];
    if (images.length === 0) {
        contents.push({ title: "No images", component: addImageButton });
    } else {
        contents = images.slice(0, 2).map((item) => ({
            title: pascalCase(item.subtype?.replace("image.", "") || ""),
            component: (
                <ImageCard
                    image={item}
                    imageOnly={true}
                    className={classes.imageCard}
                    support_color={support_color}
                />
            ),
        }));
        contents.push({
            title: "Add image",
            text: images.length === 1 ? "1 image" : `${images.length} images`,
            component: addImageButton,
        });
    }
    return <SectionSummary contents={contents} />;
};

export default ImagesSummary;
