import React from "react";

import { Typography } from "@material-ui/core";
import clsx from "clsx";

import Collection from "../../Models/Collection";
// import { findTitle } from "../../Models/Content";
import LinkQuery from "../General/LinkQuery";
import PublishStatusIcon from "../General/PublishStatusIcon";
import { RendererProps } from "../Sorter";

import { getTitleWithParent } from "./CollectionSorterRenderer.functions";
import useStyles from "./CollectionSorterRenderer.styles";

export const CollectionSorterRenderer: React.FC<RendererProps<Collection>> = ({
    item: collection,
    dragHandle,
    removeHandle,
    children,
}) => {
    const classes = useStyles();

    return (
        <div
            className={clsx(
                classes.collection,
                dragHandle ? classes.withDrag : undefined
            )}
        >
            {dragHandle}
            <Typography
                variant="body1"
                component="span"
                className={classes.title}
            >
                {(children ? children : getTitleWithParent(collection)) ||
                    "-untitled-"}
            </Typography>
            {<PublishStatusIcon item={collection} />}
            <Typography
                variant="caption"
                component="span"
                className={classes.type}
            >
                {collection.type}
            </Typography>
            {removeHandle}
        </div>
    );
};

export const CollectionLinkSorterRenderer: React.FC<RendererProps<Collection>> =
    (props) => {
        const collection = props.item;
        return (
            <CollectionSorterRenderer {...props}>
                <LinkQuery
                    toRoute="admin.collections.edit"
                    query={{ id: collection.id }}
                >
                    {getTitleWithParent(collection) || "-untitled-"}
                </LinkQuery>
            </CollectionSorterRenderer>
        );
    };

export default CollectionSorterRenderer;
