import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
        paddingRight: 0,
    },
    border: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        border: "1px solid",
        borderColor: theme.palette.divider,
        borderRadius: theme.shape.borderRadius,
        pointerEvents: "none",
        "&:hover": {
            borderColor: theme.palette.text.primary,
        },
    },
    fullscreen: {
        position: "fixed",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: theme.zIndex.drawer + 10,
        backgroundColor: theme.palette.background.paper,

        "& $editarea": {
            maxHeight: `calc(100% - ${theme.spacing(4)})`,
        },
        "& $focused": {
            borderWidth: 0,
        },
        "& $toolbar": {
            position: "relative",
        },
    },
    focused: {
        borderColor: theme.palette.primary.main,
        borderWidth: 2,
        zIndex: theme.zIndex.appBar + 5,
    },
    editarea: {
        minHeight: "20vh",
        maxHeight: "60vh",
        overflowY: "auto",
        zIndex: 10,
        paddingRight: "calc(100% - 600px)",

        "& .selected": {
            background: "rgba(200,200,200,0.5)",
            position: "relative",
        },
        "& .selected .selected": {
            // background: theme.palette.grey[300],
            position: "relative",
        },
        "& .selected .selected .selected": {
            // background: theme.palette.grey[400],
            position: "relative",
        },
        "&  > .selected::before": {
            content: "attr(data-slate-type)",
            fontStyle: "italic",
            fontSize: theme.typography.fontSize * 0.7,
            lineHeight: 1,
            color: theme.palette.text.disabled,
            position: "absolute",
            top: "-1.3em",
            backgroundColor: "rgba(255,255,255,0.5)",
        },
        "& a[data-slate-type=link]": {
            textDecoration: "underline",
            color: theme.palette.primary.main,
        },
        "& figure": {
            clear: "both",
            position: "relative",
        },
        "& figure.selected::before": {
            content: "'Actual embedded content will be shown larger'",
            fontStyle: "italic",
            color: theme.palette.primary.light,
            position: "absolute",
            top: "-1.3em",
        },
        "& figure.quote.selected::before": {
            content: "'quote'",
        },
        "& figure img": {
            maxHeight: 100,
            pointerEvents: "none",
        },
        "& figure iframe": {
            height: 100,
            pointerEvents: "none",
        },
        "& figure.float-left": {
            float: "left",
            width: "50%",
        },
        "& figure.float-right": {
            float: "right",
            width: "50%",
        },
        "& figcaption.empty": {
            position: "relative",

            "&::after": {
                content: "'caption (optional)'",
                fontStyle: "italic",
                fontSize: theme.typography.fontSize * 0.7,
                color: theme.palette.primary.light,
                position: "absolute",
                top: 0,
            },
        },
    },
    toolbar: {
        height: theme.spacing(4),
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(0),
        marginTop: theme.spacing(-0.8),
        paddingRight: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingTop: 0,
        paddingBottom: 0,
        borderColor: theme.palette.divider,
        borderStyle: "solid",
        borderWidth: 1,
        borderTopWidth: 0,
        position: "sticky",
        top: 0,
        backgroundColor: theme.palette.background.paper,
        zIndex: theme.zIndex.appBar + 1,
    },
    toolbarDisable: {},
}));

export default useStyles;
