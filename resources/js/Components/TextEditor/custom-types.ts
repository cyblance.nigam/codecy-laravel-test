import { BaseEditor, Descendant, Text } from "slate";
import { ReactEditor } from "slate-react";
import { HistoryEditor } from "slate-history";

import {
    BlockQuoteElementType,
    QuoteElementType,
    ImageElementType,
    LinkElementType,
    VideoElementType,
    ListItemElementType,
    OrderedListElementType,
    UnorderedListElementType,
} from "./Elements";
import { ItemElementType } from "./Elements/Item";
import { OEmbedElementType } from "./Elements/OEmbed";
import {
    FootnoteAnchorElementType,
    FootnoteEditor,
    FootnoteElementType,
} from "./Elements/Footnote/type";
import { IFrameElementType } from "./Elements/IFrame";

export type CaptionElementType = {
    type: "caption";
    children: (Text | CiteElementType)[];
};
export type CiteElementType = {
    type: "cite";
    children: Text[];
};
export type HeadingElementType = {
    type: "heading";
    level: 3 | 4 | 5 | 6;
    children: Text[];
};
// export type Heading3ElementType = {
//     type: "heading-3";
//     children: Text[];
// };
// export type Heading4ElementType = {
//     type: "heading-4";
//     children: Text[];
// };
// export type Heading5ElementType = {
//     type: "heading-5";
//     children: Text[];
// };
// export type Heading6ElementType = {
//     type: "heading-6";
//     children: Text[];
// };

export type ParagraphElementType = {
    type: "paragraph";
    children: Text[];
};

type ImportedElementTypes =
    | BlockQuoteElementType
    | FootnoteElementType
    | FootnoteAnchorElementType
    | IFrameElementType
    | ImageElementType
    | ItemElementType
    | LinkElementType
    | ListItemElementType
    | OEmbedElementType
    | OrderedListElementType
    | UnorderedListElementType
    | QuoteElementType
    | VideoElementType;

export type CustomElement =
    | ImportedElementTypes
    | CaptionElementType
    | CiteElementType
    | HeadingElementType
    // | Heading3ElementType
    // | Heading4ElementType
    // | Heading5ElementType
    // | Heading6ElementType
    | ParagraphElementType;

export type CustomText = {
    bold?: boolean;
    italic?: boolean;
    quote?: boolean;
    text: string;
};

export type CustomEditor = BaseEditor &
    ReactEditor &
    HistoryEditor &
    FootnoteEditor;

declare module "slate" {
    interface CustomTypes {
        Editor: CustomEditor;
        Element: CustomElement;
        Text: CustomText;
    }
}
