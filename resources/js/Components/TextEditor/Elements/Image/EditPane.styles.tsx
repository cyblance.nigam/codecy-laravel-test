import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles(() => ({
    image: {
        maxWidth: 200,
        maxHeight: 200,
    },
}));

export default useStyles;
