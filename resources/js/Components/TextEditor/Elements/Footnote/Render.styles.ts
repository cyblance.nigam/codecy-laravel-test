import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) => ({
    footnoteOrder: {
        float: "left",
        paddingRight: theme.spacing(1),
    },
}));

export default useStyles;
