import React from "react";

import clsx from "clsx";
import { RenderElementProps, useSelected } from "slate-react";

const QuoteRender: React.FC<RenderElementProps> = ({
    attributes,
    element,
    children,
}) => {
    const selected = useSelected();
    if (element.type !== "quote") {
        return <></>;
    }
    const { float } = element;
    const floatClass = float ? `float-${float}` : undefined;

    return (
        <figure
            {...attributes}
            className={clsx("quote", floatClass, { selected: selected })}
        >
            {children}
        </figure>
    );
};

export default QuoteRender;
