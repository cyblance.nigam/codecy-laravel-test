import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles(() => ({
    linkPreview: {
        width: 400,
    },
}));

export default useStyles;
