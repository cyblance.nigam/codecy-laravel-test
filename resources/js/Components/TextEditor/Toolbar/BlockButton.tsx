import React from "react";

import { IconButton, Tooltip } from "@material-ui/core";
import clsx from "clsx";
import { Editor, Element as SlateElement } from "slate";
import { useSlate } from "slate-react";

import { isBlockActive, toggleBlock } from "./BlockButton.functions";
import useStyles from "./Toolbar.styles";

export type BlockElementTypes =
    | "paragraph"
    | "ordered-list"
    | "unordered-list"
    // | "list-item"
    | "quote"
    | "heading"
    // | "heading-3"
    // | "heading-4"
    // | "heading-5"
    // | "heading-6"
    | "footnote"
    | "footnote-anchor";

interface IBlockButtonProps {
    format: BlockElementTypes;
    attributes?: Partial<SlateElement>;
    removeBlockFunction?: (editor: Editor) => void;
    tooltip?: string;
}
const BlockButton: React.FC<IBlockButtonProps> = ({
    format,
    removeBlockFunction,
    attributes,
    tooltip,
    children,
}) => {
    const editor = useSlate();
    const classes = useStyles();
    const active = isBlockActive(editor, format, attributes);
    const activeBase = isBlockActive(editor, format);
    const isParagraph = isBlockActive(editor, "paragraph");

    const btn = (
        <IconButton
            className={clsx(classes.button, { [classes.buttonActive]: active })}
            disabled={!isParagraph && !activeBase}
            onMouseDown={(evt) => {
                evt.preventDefault();
                evt.stopPropagation();
                toggleBlock(editor, format, attributes, removeBlockFunction);
            }}
            size="small"
        >
            {children}
        </IconButton>
    );
    if (tooltip) {
        return (
            <Tooltip title={tooltip} placement="top">
                <span>{btn}</span>
            </Tooltip>
        );
    } else {
        return btn;
    }
};
export default BlockButton;
