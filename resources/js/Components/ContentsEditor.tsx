import React, {
    PropsWithChildren,
    useCallback,
    useEffect,
    useMemo,
    useReducer,
    useState,
} from "react";

import {
    FormControl,
    InputLabel,
    Typography,
    Box,
    Paper,
    Theme,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles } from "@material-ui/styles";
import { pascalCase } from "pascal-case";
import { useList } from "react-use";

import { ItemSubtype } from "../Config";
import Content from "../Models/Content";
// import { useDispatch } from "../Stores/DispatchProvider";
import contentsReducer from "../Stores/contentsReducer";
// import useUpdateContent from "../Stores/useUpdateContent";
import { LanguageProvider } from "../Utils/LanguageContext";

import ButtonConfirmDialog from "./General/ButtonConfirmDialog";
import DebounceTextField from "./General/DebounceTextField";
import LanguageTabs from "./General/LanguageTabs";
import TabPanel from "./General/TabPanel";
import TextEditor from "./TextEditor";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
    },
    tabs: {
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(-2),
    },
    container: {
        paddingRight: theme.spacing(2),
        paddingLeft: theme.spacing(2),
    },
    labelOnOutline: {
        backgroundColor: theme.palette.background.paper,
        paddingLeft: 5,
        marginLeft: -5,
        paddingRight: 5,
        marginRight: -5,
        zIndex: `${theme.zIndex.appBar + 10} !important` as unknown as number,
    },
}));

export interface EditorProps<T> {
    content: T;
    itemSubtype: ItemSubtype | undefined;
    onChange: (field: string, value: any) => void;
    onvalid1?: boolean
}

interface IProps<T> {
    contents: T[];
    fields: string[];
    htmlFields?: { [key: string]: "full" | "limited" };
    itemSubtype?: ItemSubtype;
    additionalEditor?: React.ComponentType<EditorProps<T>>;
    onChange: (contents: T[]) => void;
    onvalid?:boolean;
   
}
const ContentsEditor = <T extends Content>({
    contents: _contents,
    // contents,
    fields: _fields,
    htmlFields: _htmlFields,
    itemSubtype,
    additionalEditor: AdditionalEditor,
    onChange,
    onvalid,
}: // onChange,
PropsWithChildren<IProps<T>>) => {
    const classes = useStyles();
    // const [contents, setContents] = useState<T[]>(_contents);
    // const [lang, setLang] = useState(_contents[0]?.lang);
    const [lang, setLang] = useState(_contents[0]?.lang);
    // const updateContent = useUpdateContent();
    // const dispatch = useDispatch();
    const [contents, dispatch] = useReducer(contentsReducer<T>(), _contents);
    const [fields] = useList(_fields);
    const [htmlFields] = useState(_htmlFields);

    const handleFieldChangeMap = useMemo(() => {
        const handlers = new Map<string, (value: any) => void>();
        if (!lang) {
            return handlers;
        }
        const fieldsToHandle = [
            ...fields,
            ...Object.keys(htmlFields || []).map((field) => `${field}_json`),
        ];
        fieldsToHandle.forEach((field) =>
            handlers.set(field, (valueOrEvent: any) => {
                const value = valueOrEvent?.target?.value
                    ? valueOrEvent.target.value
                    : valueOrEvent;
                dispatch({ type: "content_update", field, value, lang });
            })
        );
        return handlers;
    }, [fields, htmlFields, lang]);

    const handleFieldChange = useCallback(
        (field, value) => {
            dispatch({ type: "content_update", field, value, lang: lang! });
        },
        [lang]
    );
    // const onChangeField = (
    //     lang: string,
    //     field: string,
    //     value: string,
    //     // emitChange = false
    // ) => {
    //     const newContents = contents.map((content) =>
    //         content.lang !== lang ? content : { ...content, [field]: value }
    //     );
    //     // setContents(newContents);
    //     // if (emitChange) {
    //         // onChange && onChange(newContents);
    //         updateContent(lang, field, value);
    //     }
    // };

    // const onBlur = () => {
    //     if (contents !== _contents) {
    //         onChange && onChange(contents);
    //         // updateContent(lang, )
    //     }
    // };

    // const onAddedLanguage = (contents: T[], addedLang: string) => {
    const onAddLanguage = (addedLang: string) => {
        // setContents(contents);
        dispatch({
            type: "content_add",
            lang: addedLang,
            content: contents.find((content) => content.lang === lang),
            fields,
        });
        setLang(addedLang);
        // onChange && onChange(contents);
    };

    const removeLanguage = (removedLang: string) => {
        const newContents = contents.filter(
            (content) => content.lang !== removedLang
        );
        if (!newContents.length) {
            console.log("don't allow remove all");
            return;
        }
        // setContents(newContents);
        if (lang === removedLang) {
            setLang(newContents[0]?.lang);
        }
        // onChange && onChange(newContents);
        dispatch({ type: "content_remove", lang: removedLang });
    };

    useEffect(() => {
        // console.log("dispatch content_reset", _contents[0]);
        const timer = setTimeout(() => {
            // console.log("actually do the change", _contents[0]);
            dispatch({ type: "content_reset", contents: _contents });
            setLang((lang) => {
                if (_contents.find((c) => c.lang === lang)) {
                    return lang;
                } else {
                    return _contents.length ? _contents[0].lang : undefined;
                }
            });
        }, 100);
        return () => clearTimeout(timer);
    }, [_contents]);

    useEffect(() => {
        onChange(contents);
    }, [onChange, contents]);

    return (
        <Paper className={classes.container}>
            {!contents?.length && (
                <Typography color="warning" variant="body1">
                    No contents yet, add a language
                </Typography>
            )}
            <LanguageProvider language={lang}>
                <LanguageTabs
                    onChange={setLang}
                    // onAdded={onAddedLanguage}
                    onAdd={onAddLanguage}
                    language={lang}
                    contents={contents}
                    showAdd
                />
                {contents.map((content, i) => (
                    <TabPanel show={content.lang === lang} key={i}>
                        <Box display="flex" flexDirection="row-reverse">
                            <ButtonConfirmDialog
                                label={`Remove this language variant (${lang})`}
                                color="secondary"
                                onConfirm={() => removeLanguage(content.lang)}
                                icon={<DeleteIcon />}
                                buttonProps={{
                                    size: "small",
                                    disabled: contents.length === 1,
                                }}
                            >
                                Removing cannot be undone.
                            </ButtonConfirmDialog>
                        </Box>
                        <Box display="flex" flexDirection="column">
                            {fields.map((field, j) =>
                                htmlFields && htmlFields[field] ? undefined : (
                                    <DebounceTextField
                                        key={j}
                                        label={pascalCase(field)}
                                        value={(content as any)[field] || ""}
                                        onChange={handleFieldChangeMap.get(
                                            field
                                        )}
                                    />
                                )
                            )}
                            {AdditionalEditor && (
                                <>
                                    <AdditionalEditor
                                        content={content}
                                        itemSubtype={itemSubtype}
                                        onChange={handleFieldChange}
                                    />
                                    <Typography variant="h6">
                                        Content
                                    </Typography>
                                </>
                            )}
                            {fields.map((field, j) =>
                                htmlFields && htmlFields[field] ? (
                                    <FormControl
                                        key={j}
                                        variant="outlined"
                                        fullWidth
                                    >
                                        <InputLabel
                                            className={classes.labelOnOutline}
                                            shrink
                                            // style={{ backgroundColor: "white" }}
                                        >
                                            {pascalCase(field)}
                                        </InputLabel>
                                        <TextEditor
                                            text={(content as any)[field]}
                                            json={
                                                (content as any)[
                                                    `${field}_json`
                                                ]
                                            }
                                            mode={htmlFields[field]}
                                            // onChange={
                                            //     handleFieldChangeMap.get(field)!
                                            // }
                                            onChangeJson={
                                                handleFieldChangeMap.get(
                                                    `${field}_json`
                                                )!
                                            }
                                        />
                                    </FormControl>
                                ) : undefined
                            )}
                        </Box>
                    </TabPanel>
                ))}
            </LanguageProvider>
        </Paper>
    );
};

export default ContentsEditor;
