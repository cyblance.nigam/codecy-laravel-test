import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    switchGroup: {
        marginTop: theme.spacing(1) + 1,
    },
    switch: {
        marginTop: theme.spacing(2) + 1,
        marginBottom: theme.spacing(1) + 1,
    },
}));

export default useStyles;
