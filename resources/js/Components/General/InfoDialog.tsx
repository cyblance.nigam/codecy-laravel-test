import React from "react";

import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from "@material-ui/core";

interface IProps {
    open: boolean;
    title: string;
    text?: string;
    label?: string;
    onClose: () => void;
}
const InfoDialog: React.FC<IProps> = ({
    open,
    title,
    text,
    label,
    children,
    onClose,
}) => {
    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <DialogContentText>{text}</DialogContentText>
                {children}
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} autoFocus>
                    {label ?? "Ok"}
                </Button>
            </DialogActions>
        </Dialog>
    );
};
export default InfoDialog;
