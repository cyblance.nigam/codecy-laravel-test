import React, { PropsWithChildren, useEffect, useState } from "react";

import { Theme } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import { makeStyles } from "@material-ui/styles";

import { AvailableLanguages } from "../../Config";
import Content from "../../Models/Content";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
    },
    tabs: {
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(0),
    },
    tabDense: {
        minWidth: theme.spacing(2),
    },
    addButton: {},
}));

// type Content = ItemContent | CollectionContent;
interface IProps<T> {
    contents: T[];
    language?: string;
    showAdd?: boolean;
    dense?: boolean;
    onChange: (lang: string) => void;
    // onAdded: (contents: T[], lang: string) => void;
    onAdd: (lang: string) => void;
}
const LanguageTabs = <T extends Content>({
    contents,
    language,
    showAdd,
    dense,
    onChange,
    onAdd,
}: PropsWithChildren<IProps<T>>) => {
    const classes = useStyles();
    const [languages, setLanguages] = useState<string[]>([]);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const onClickAdd = (evt: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(evt.currentTarget);
    };

    const onClickMenuItem = (lang?: string) => {
        setAnchorEl(null);
        // lang && addLanguage(lang);
        lang && onAdd(lang);
    };

    const activeLanguage =
        language && languages.includes(language) ? language : languages[0];

    // const addLanguage = (addedLang: string) => {
    //     const content =
    //         contents.find((content) => content.lang === language) || ({} as T);
    //     const newContent = { ...content, lang: addedLang, id: undefined };
    //     // shallow clone and remove everything that looks like an id
    //     for (const [k, v] of Object.entries(newContent)) {
    //         if (Array.isArray(v)) {
    //             const vNoId = v.map((obj) => {
    //                 const newObj = { ...obj };
    //                 delete newObj.id;
    //                 delete newObj.content_id;
    //                 delete newObj.item_content_id;
    //                 return newObj;
    //             });
    //             (newContent as any)[k] = vNoId;
    //         }
    //     }
    //     const newContents = [...contents, newContent];
    //     onAdded(newContents, addedLang);
    // };

    useEffect(() => {
        setLanguages(contents.map((content) => content.lang));
    }, [contents]);

    return (
        <Grid container alignItems="center" className={classes.tabs}>
            {showAdd && (
                <Grid item xs={1}>
                    <Button
                        className={classes.addButton}
                        color="secondary"
                        onClick={onClickAdd}
                        size="small"
                        startIcon={<AddCircleIcon />}
                    >
                        language
                    </Button>
                    <Menu
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={() => onClickMenuItem()}
                    >
                        {AvailableLanguages.map(([code, name], i) => (
                            <MenuItem
                                onClick={() => onClickMenuItem(code)}
                                disabled={languages.indexOf(code) >= 0}
                                key={i}
                            >
                                {name}
                            </MenuItem>
                        ))}
                    </Menu>
                </Grid>
            )}
            {languages.length ? (
                <Grid item xs={10}>
                    <Tabs
                        value={activeLanguage}
                        onChange={(e, newLang) => onChange(newLang)}
                        centered
                    >
                        {languages.map((lang, i) => (
                            <Tab
                                value={lang}
                                label={lang}
                                key={i}
                                className={dense ? classes.tabDense : ""}
                            />
                        ))}
                    </Tabs>
                </Grid>
            ) : (
                ""
            )}
        </Grid>
    );
};

export default LanguageTabs;
