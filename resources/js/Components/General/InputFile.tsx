import React, { ChangeEventHandler, forwardRef } from "react";

import { OutlinedInput } from "@material-ui/core";
import clsx from "clsx";

import useStyles from "./InputFile.styles";

interface IProps {
    label?: string;
    className?: string;
    accept?: string;
    multiple?: boolean;
    onChange: ChangeEventHandler<HTMLInputElement>;
}
const InputFile = forwardRef<HTMLInputElement, IProps>(
    ({ label, className, onChange, multiple, accept }, ref) => {
        const classes = useStyles();

        const _onChange: ChangeEventHandler<HTMLInputElement> = (e) => {
            onChange(e);
        };

        return (
            <OutlinedInput
                className={clsx(classes.input, className)}
                type="file"
                inputRef={ref}
                label={label}
                onChange={_onChange}
                inputProps={{ accept, multiple }}
                fullWidth
                notched
            />
        );
    }
);

export default InputFile;
