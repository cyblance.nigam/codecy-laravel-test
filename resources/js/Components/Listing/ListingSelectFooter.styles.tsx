import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    button: {
        marginRight: theme.spacing(2),
    },
}));

export default useStyles;
