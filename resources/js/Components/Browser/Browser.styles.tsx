import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    dialogContent: {
        paddingTop: 0,
        height: "100%",
        minHeight: "25vh",
        minWidth: "25vw",
        maxWidth: "40vw",
        boxSizing: "border-box",
        overflowY: "auto",
        display: "flex",
        flexDirection: "column",
    },
    tallDialog: {
        minHeight: "80vh",
        maxHeight: "unset",
    },
    wideDialog: {
        minWidth: "90vw",
        maxWidth: "unset",
    },
    dialogContainer: {
        padding: theme.spacing(2),
    },
}));

export default useStyles;
