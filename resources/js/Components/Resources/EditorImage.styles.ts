import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    root: {
        // maxHeight: "100px",
    },
    imageCard: {
        width: "100%",
        maxWidth: 350,
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(1),
    },
}));

export default useStyles;
