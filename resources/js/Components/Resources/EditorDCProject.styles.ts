import { Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
    },
    tabs: {
        marginBottom: theme.spacing(1),
        marginTop: theme.spacing(-2),
    },
    container: {
        paddingRight: theme.spacing(2),
        paddingLeft: theme.spacing(2),
    },
    labelOnOutline: {
        backgroundColor: theme.palette.background.paper,
        paddingLeft: 5,
        marginLeft: -5,
        paddingRight: 5,
        marginRight: -5,
        zIndex: theme.zIndex.appBar + 10,
    },
}));

export default useStyles;
