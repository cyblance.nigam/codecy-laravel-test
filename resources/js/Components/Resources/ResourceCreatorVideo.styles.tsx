import { makeStyles } from "@material-ui/styles";
const useStyles = makeStyles(() => ({
    container: {
        minWidth: 500,
    },
}));

export default useStyles;
