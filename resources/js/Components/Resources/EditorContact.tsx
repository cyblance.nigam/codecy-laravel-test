import React, { useEffect, useState } from "react";

import { Grid, TextField, Typography } from "@material-ui/core";
import { pascalCase } from "pascal-case";

import { Contact, ItemContent } from "../../Models";
import { EditorProps } from "../ContentsEditor";

import { CONTACT_FIELDS } from "./EditorContact.constants";

const EditorContact: React.FC<EditorProps<ItemContent>> = ({
    content,
    onChange,
    onvalid1,
}) => {
    const [contact, setContact] = useState<Contact | undefined>(
        content?.contacts && content?.contacts[0]
            ? content?.contacts[0]
            : undefined
    );
    const [value, setonvalue] = useState(false);

    // Validation Added By Cyblance 
    const [isValid, setIsValid] = useState(false);
    
    const initialErrors = {
        PhoneMain: "",
        PhoneOther: "",
        PhoneOther2: "",
        FaxMain: "",
        FaxOther: "",
        Website: "",
        Email: "",
        EmailOther: "",
        Street1: "",
        Street2: "",
        Street3: "",
        Zip: "",
        State: "",
        CountryCode: "",
        City: "",
    };

    useEffect(() => {
        handleChange1();
        setContact(content?.contacts[0]);
    }, [content.contacts]);

    const [errors, setErrors] = useState(initialErrors);
    function handleChange1() {
        let PhoneMain: string = "";
        let PhoneOther: string = "";
        let PhoneOther2: string = "";
        let FaxMain: string = "";
        let FaxOther: string = "";
        let Website: string = "";
        let Email: string = "";
        let EmailOther: string = "";
        let Street1: string = "";
        let Street2: string = "";
        let Street3: string = "";
        let Zip: string = "";
        let City: string = "";
        let State: string = "";
        let CountryCode: string = "";
        const abc = content?.contacts[0];
        City = abc.city ? "" : "city required field.";
        State = abc.state ? "" : "state required field.";
        CountryCode = abc.country_code ? "" : "country code required field.";
        Email = abc.email ? "" : "email required field.";
        EmailOther = abc.email_other ? "" : "email other required field.";
        FaxMain = abc.fax_main ? "" : "fax main required field.";

        FaxOther = abc.fax_other ? "" : "fax other required field.";
        PhoneMain = abc.phone_main ? "" : "phone main required field.";
        PhoneOther = abc.phone_other ? "" : "phone other required field.";
        PhoneOther2 = abc.phone_other2 ? "" : "phone other 2 required field.";

        Street1 = abc.street1 ? "" : "street1 required field.";
        Street2 = abc.street2 ? "" : "street2 required field.";
        Street3 = abc.street3 ? "" : "street3 required field.";
        Website = abc.website ? "" : "website required field.";
        Zip = abc.zip ? "" : "zip required field.";

        //phone1 validation
        if (abc.phone_main != '') {
            if (typeof abc.phone_main !== "undefined") {
                var pattern = new RegExp(/^[0-9\b]+$/);
                if (!pattern.test(abc.phone_main)) {
                    setIsValid(false);
                    PhoneMain = "Please enter only number.";
                } else if (abc.phone_main.length != 10 || abc.phone_main.length > 10) {
                    setIsValid(false);
                    PhoneMain = "Please enter valid phone number.";
                }else{
                    setIsValid(true);
                }
            }else{
                setIsValid(true);
            }
            
        }
        
       
        //phone2 validation
        if (abc.phone_other != '') {
            if (typeof abc.phone_other !== "undefined") {
                var pattern = new RegExp(/^[0-9\b]+$/);
                if (!pattern.test(abc.phone_other)) {
                   
                    PhoneOther = "Please enter only number.";
                } else if (abc.phone_other.length != 10) {
                   
                    PhoneOther = "Please enter valid phone number.";
                }
            }
        }
        //phone3 validation
        if (abc.phone_other2 != '') {
            if (typeof abc.phone_other2 !== "undefined") {
                var pattern = new RegExp(/^[0-9\b]+$/);
                if (!pattern.test(abc.phone_other2)) {
                    PhoneOther2 = "Please enter only number.";
                } else if (abc.phone_other2.length != 10) {
                    PhoneOther2 = "Please enter valid phone number.";
                }

            }
        }
        var regex = new RegExp("^\\+[0-9]{1,3}-[0-9]{3}-[0-9]{7}$");
        if (abc.fax_main != '') {
            if (!regex.test(abc.fax_main)) {
                FaxMain = 'Fax no is invalid';
            }
        }

        if (abc.fax_other != '') {
            if (!regex.test(abc.fax_other)) {
                FaxOther = 'Fax no is invalid';
            }
        }
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i');

        if (abc.website != '') {
            if (!pattern.test(abc.website)) {
                Website = 'enter valid url';
            }
        }

        var pattern1 = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

        if (abc.email != '') {
            if (!pattern1.test(abc.email)) {
                Email = "enter valid email address.";
            }else{
            }
        }

        if (abc.email_other != '') {
            if (!pattern1.test(abc.email_other)) {
                EmailOther = "enter valid email other address.";
            }
            else{
                 
            }
        }

        setErrors((prevState) => ({
            ...prevState,
            City,
            PhoneMain,
            PhoneOther,
            PhoneOther2,
            FaxMain,
            FaxOther,
            Website,
            Email,
            EmailOther,
            Street1,
            Street2,
            Street3,
            Zip,
            State,
            CountryCode,
        }));
    }

    const setField = (name: string, value: string) => {
        setContact((contact) => ({ ...contact, [name]: value } as Contact));
    };
    
    const onBlur = () => {
        console.log("validation Fromn"+isValid);
        if (contact !== content?.contacts[0]) {
            onChange("contacts", [contact]);
        }
    };

    if (!contact) {
        return <Typography>No contact details found.</Typography>;
    }

    return (
        <Grid container spacing={1}>
            <Grid item xs={12}>
                <Typography variant="h6">Contact card</Typography>
            </Grid>
            {CONTACT_FIELDS.map((field, idx) => (
                <Grid item xs={4} key={idx}>
                    <TextField
                        value={((contact as any)[field] as string) ?? ""}
                        label={pascalCase(field)}
                        variant="outlined"
                        fullWidth
                        onChange={(e) =>
                            setField(field, e.target.value as string)
                        }
                        onBlur={onBlur}
                        margin="dense"
                        InputLabelProps={{ shrink: true }}
                        {...pascalCase(field) == 'City' ? { helperText: errors.City } : ''}
                        {...pascalCase(field) == 'PhoneMain' ? { helperText: errors.PhoneMain } : ''}
                        {...pascalCase(field) == 'PhoneOther' ? { helperText: errors.PhoneOther } : ''}
                        {...pascalCase(field) == 'PhoneOther2' ? { helperText: errors.PhoneOther2 } : ''}
                        {...pascalCase(field) == 'FaxMain' ? { helperText: errors.FaxMain } : ''}
                        {...pascalCase(field) == 'FaxOther' ? { helperText: errors.FaxOther } : ''}
                        {...pascalCase(field) == 'Website' ? { helperText: errors.Website } : ''}
                        {...pascalCase(field) == 'Email' ? { helperText: errors.Email } : ''}
                        {...pascalCase(field) == 'EmailOther' ? { helperText: errors.EmailOther } : ''}
                        {...pascalCase(field) == 'Street1' ? { helperText: errors.Street1 } : ''}
                        {...pascalCase(field) == 'Street2' ? { helperText: errors.Street2 } : ''}
                        {...pascalCase(field) == 'Street3' ? { helperText: errors.Street3 } : ''}
                        {...pascalCase(field) == 'Zip' ? { helperText: errors.Zip } : ''}
                        {...pascalCase(field) == 'State' ? { helperText: errors.State } : ''}
                        {...pascalCase(field) == 'CountryCode' ? { helperText: errors.CountryCode } : ''}
                    />
                </Grid>
            ))}
        </Grid>
    );
};

export default EditorContact;
