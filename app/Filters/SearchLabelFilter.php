<?php 
namespace App\Filters;
use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class SearchLabelFilter implements Filter{

    public function __invoke(Builder $query, $value, string $property){
        return $query->where('name', 'like', "%$value%")
                ->orWhere('id', 'like', "%$value%")
                ->orWhere('is_active', 'like', "%$value%");
    }
}
