<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Social_media extends Model
{
    use HasFactory, Notifiable;
    public $timestamps = false;    
    protected $table = 'social_media';    
    protected $guarded = [];
}
