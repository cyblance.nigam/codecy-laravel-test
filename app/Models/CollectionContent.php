<?php

namespace App\Models;

use App\Scopes\LanguageScope;
use Illuminate\Support\Str;
//Added By Cyblance for delete functionality
use Illuminate\Database\Eloquent\SoftDeletes;

class CollectionContent extends Model
{
    use SoftDeletes;
    protected $touches = ['collection'];
    protected $guarded = ['content'];
    //Added By Cyblance for delete functionality
    protected $dates = [
        'deleted_at'
    ];
    
    protected static function booted() {
        static::addGlobalScope(new LanguageScope);
    }

    public function setTitleAttribute($val) {
        $this->attributes['title'] = $val;
        // always override slug
        $this->attributes['slug'] = Str::slug($val);
    }

    public function collection() {
        return $this->belongsTo('App\Models\Collection');
    }

}
