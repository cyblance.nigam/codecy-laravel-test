<?php 

namespace App\Actions;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

use Embera\Cache\Filesystem;
use Embera\Http\HttpClient;
use Embera\Http\HttpClientCache;
use Embera\Embera;
 
use Embed\Embed;
use Exception;

class OEmbed {

    public static function embed($text) {
        $text = Str::of($text)
            ->replaceMatches(
                '#<section\s+class="social-embed"\s+data-url="([^"]+)"[^>]*>[^>]*</section>#im', 
                function($match) {
                    // $data = $embed->get($match[1]);
                    $data = OEmbed::load($match[1]);
                    $html = '';
                    try {
                        $html = isset($data->code->html) ? $data->code->html : '';
                    } catch (Exception $ex) {
                        print_r($ex);
                    }
                    return $html ? '<section class="social-embed social-embed-'.$data->providerName.'" data-url="'.$match[1].'">' 
                                    .$html.'</section>'
                                : '';
                }
            );

        return $text;
    }

    public static function load($url) {
        $info = Cache::rememberForever('oembed_'.$url, function() use ($url) {
            $embed = new Embed();
            $embed->setSettings([
                'facebook:token' => config('eiie.oembed-settings')['facebook:token']
            ]);
            $info = $embed->get($url);
            $oembed = $info->getOEmbed();
            $title = $oembed->str('title');
            $all = $oembed->all();
            return $info;
        });
        return $info;
    }

    // public static function emberaEmbed($text) {
    //     Storage::disk('cache')->createDir('.embera');
    //     $writablePath = Storage::disk('cache')->path('.embera'); // Your writable Path
    //     $duration = 3600; // Duration of the cache in seconds
        
    //     $httpCache = new HttpClientCache(new HttpClient());
    //     $httpCache->setCachingEngine(new Filesystem($writablePath, $duration));
        
    //     // Instantiate Embera
    //     $embera = new Embera([], null, $httpCache);

    //     $text = Str::of($text)
    //         ->replaceMatches(
    //             '#<section\s+class="social-embed"\s+data-url="([^"]+)"[^>]*>[^>]*</section>#im', 
    //             function($match) use ($embera) {
    //                 $data = $embera->getUrlData($match[1]);
    //                 echo $match[1];
    //                 $html = isset($data[$match[1]]['html']) ? $data[$match[1]]['html'] : '';
    //                 return $html ? '<section class="social-embed" data-url="'.$match[1].'">' 
    //                                 .$html.'</section>'
    //                             : '';
    //             }
    //         );

    //     return $text;
    // }

}