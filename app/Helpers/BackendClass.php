<?php 
use App\Models\Social_media;


//Function Add  for generate success messege for both Item and Collection Module by Cyblance
function item_success_message($type,$subtype=null){

    if($type == 'article'){
        $getCreateMsg = 'Article';       
    }elseif($type == 'static'){
        $getCreateMsg = 'Page';       
    }elseif($type == 'resource') {  
        if($subtype == 'image.icon' || $subtype == 'image.square' || $subtype == 'image.portrait' || $subtype == 'image'){
            $getCreateMsg='Image';
        }else{
            $getCreateMsg = ucfirst($subtype);
        } 
    }elseif ($type == 'contact') {
        $getCreateMsg = 'Contact card';
    }elseif ($type == 'dcproject') {
        $getCreateMsg = 'DC Project';
    }elseif ($type == 'articles') {
        $getCreateMsg = 'Blog series';
    }elseif ($type == 'dossier_sub') {
        $getCreateMsg = 'Dossier Subsections';
    }elseif ($type == 'contacts') {
        $getCreateMsg = 'Contact cards';
    }else {
        $getCreateMsg = ucfirst($type);
    }
    return $getCreateMsg;
}

function labels(){

    if(request()->segment(1)==''){
        $langAliase = 'en';
    }else{
        $langAliase = request()->segment(1); 
    }

    /*if($langAliase == request()->segment(1)){
        $langAliase = 'en';
    }*/

    $langid=\App\Models\Language::where('alias_name',$langAliase)->first()->id;
    $fetch_labels_array = DB::table('labels')->leftJoin('label_translates','labels.id', '=', 'label_translates.label_id')
        ->select('labels.id as LabelID', 'labels.name', 'label_translates.*')
        ->where('labels.is_active', '=', 'active')->where('label_translates.language_id', '=', $langid)->get();


    foreach($fetch_labels_array as $labels){
        $fetch_labels[$labels->name]=$labels->label_translate;
    }
    return $fetch_labels;          
}

// Translation added By cyblance 
 
function com_create_guid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function translate_support_language(){
    $langArr = array(1,2,3);
    return $langArr;
}

function active_language_translate_tag($langid,$array_tran){

    $target = $langid; 
    $api_array = array();
    $data = array();
    $newapiarray1 = array();
    $newapiarray2 = array();
    foreach ($array_tran as $key => $value) {
        $api_array[]['Text'] = $value;
    }

    if(count($api_array)>100){
        $break_api_array = array_chunk($api_array, 100);
        foreach($break_api_array as $array_tra){
            $strln = 0;
            foreach ($array_tra as $value) {
                $strln = $strln + strlen($value['Text']);
                if($strln<5000){
                    $newapiarray1[] = $value;
                }else{
                    $newapiarray2[] = $value;
                }
            }
            if(!empty($newapiarray1)){
                //For Microsoft translate
                $key = '2a70361ed47e41d0bc5098cebce880d9';
                $host = "https://api.cognitive.microsofttranslator.com";
                $path = "/translate?api-version=3.0";
                $params = "&to=".$target."&textType=html";
                $content = json_encode($newapiarray1);
                $headers = "Content-type: application/json\r\n" .
                "Content-length: " . strlen($content) . "\r\n" .
                "Ocp-Apim-Subscription-Key: $key\r\n" .
                "X-ClientTraceId: " . com_create_guid() . "\r\n";
                // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
                // http://php.net/manual/en/function.stream-context-create.php
                $options = array (
                    'http' => array (
                        'header' => $headers,
                        'method' => 'POST',
                        'content' => $content,
                        'ignore_errors' => true
                    )
                );
                $context  = stream_context_create ($options);
                $result = file_get_contents ($host . $path . $params, false, $context);
                $json = json_decode($result);

                if($json){
                    foreach ($json as $key => $value) {
                        $data[] = $value->translations[0]->text;
                    }
                }else{
                    $data['error'] = $json->error->code; 
                }
                unset($newapiarray1);
            }
            if(!empty($newapiarray2)){
                //For Microsoft translate
                $key = '2a70361ed47e41d0bc5098cebce880d9';
                $host = "https://api.cognitive.microsofttranslator.com";
                $path = "/translate?api-version=3.0";
                $params = "&to=".$target."&textType=html";
                $content = json_encode($newapiarray2);
                $headers = "Content-type: application/json\r\n" .
                "Content-length: " . strlen($content) . "\r\n" .
                "Ocp-Apim-Subscription-Key: $key\r\n" .
                "X-ClientTraceId: " . com_create_guid() . "\r\n";
                // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
                // http://php.net/manual/en/function.stream-context-create.php
                $options = array (
                    'http' => array (
                        'header' => $headers,
                        'method' => 'POST',
                        'content' => $content,
                        'ignore_errors' => true
                    )
                );
                $context  = stream_context_create ($options);
                $result = file_get_contents ($host . $path . $params, false, $context);
                $json = json_decode($result);

                if($json->error){
                    foreach ($json as $key => $value) {
                        $data[] = $value->translations[0]->text;
                    }
                }else{
                   $data['error'] = $json->error->code; 
                }
                unset($newapiarray2);
            }
        }
    }else{

        $key = '2a70361ed47e41d0bc5098cebce880d9';
        $host = "https://api.cognitive.microsofttranslator.com";
        $path = "/translate?api-version=3.0";
        $params = "&to=".$target."&textType=html";
        $content = json_encode($api_array);
        $headers = "Content-type: application/json\r\n" .
        "Content-length: " . strlen($content) . "\r\n" .
        "Ocp-Apim-Subscription-Key: $key\r\n" .
        "X-ClientTraceId: " . com_create_guid() . "\r\n";
        // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
        // http://php.net/manual/en/function.stream-context-create.php
        $options = array (
            'http' => array (
                'header' => $headers,
                'method' => 'POST',
                'content' => $content,
                'ignore_errors' => true
            )
        );
        $context  = stream_context_create ($options);
        $result = file_get_contents ($host . $path . $params, false, $context);
        $json = json_decode($result);
        $data = array();

        if($json){
            foreach ($json as $key => $value) {
                $data[] = $value->translations[0]->text;
            }
        }else{
            $data['error'] = $json->error->code;
        }
    }
    return $data;exit();
}

function getCurrentHost(){
    return request()->getHost();
}

function linkedin_token(){
    $token=get_social_access_token('linkedin');
  
    $current_date=date('Y-m-d');
    $expire_date=date("Y-m-d", strtotime($token->token_expire_date) );
    //echo "<pre>"; print_r($current_date);
    //echo "<pre>"; print_r($expire_date); exit();
    if($expire_date<=$current_date){
        //echo "in if"; exit();
        //Auth code
        // Token Get
        $curl = curl_init();    
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.linkedin.com/oauth/v2/accessToken',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'grant_type=authorization_code&code=AQR39X0R6NTuh__VnBvNLlwN8By9YC6o_ekB75pnhuGBIEJeiylymIehF4X5nnxIbPmwUEe3ZIIJNKV7fl4jhJ8PofD4yjyyt3mWqQoG1RUcwsOzagC2MaP7KY5RCSTFe_URQIrElYjEq8zSbZIwRd1-syGgU1ONb0l8vfF34JZMlGJ9VyvH-NE_ILKp0wI56mADY3fCTHkdHkHv_Sk&redirect_uri=http%3A%2F%2Flocalhost%2Flaravel-linkedin-share-master%2Fcallback.php&client_id=78uolophc4fv21&client_secret=hEhucTSmMP4M3EkJ',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded',
            'Cookie: lang=v=2&lang=en-us; bcookie="v=2&a7062a3c-d47e-40b7-8442-e0cd21fb5c7a"; bscookie="v=1&2021021610221570187e30-1b40-40ce-8a8c-cb492481827fAQGGHGvdBVvR_BlFdOV7M75kH5lANm_i"; PLAY_SESSION=eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImZsb3dUcmFja2luZ0lkIjoiQVdPODIzZkxSTnkrQ2M0NGJjRy8xdz09In0sIm5iZiI6MTYxMzQ3MTM3OCwiaWF0IjoxNjEzNDcxMzc4fQ.W42D3sj7ypqs5prtFj_wqQRiCtHGvOD69tkeAWIhaoY; JSESSIONID=ajax:3234903145390384035; lidc="b=VB17:s=V:r=V:g=3713:u=17:i=1613471959:t=1613556192:v=1:sig=AQHDtd5okGGuOMd-dGdSd3AwNwleEkzp"'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $curl_token=json_decode($response);
       // echo "<pre>"; print_r($response);
        //echo "<pre>"; print_r($curl_token->access_token); exit();
        $expire_date=date('Y-m-d h:i:s', strtotime("+59 days"));
        Social_media::where('name', 'linkedin')->update(['token' => $curl_token->access_token,'token_issue_date'=>$current_date,'token_expire_date'=>$expire_date]);
        $token=$curl_token->access_token;
    }else{
        $token=$token->token;
    }
    return $token;
}

function likendenShare($likendenData){

    $access_token = "AQXVd2brd_9ODlV0xfn3Nb_C_TzHD2zir43EU04lcQRB7SchXVVK81S4RT4VGPf7VLszgUSrv2hTN2nDaKhquRjGov7CEz9YwgIx1yLw068-lJcbqxeMwwz4a6YwkeU6v8J5X0kFE3HQU6zTeGoio3vSWYAK4CcYJYBdgeRS1lGYKn2dKGAOGhBebZ7GugjTXogEDgr8f_q5ixW077fFhizxGK0zqPyfLpvOvxnNXMwfh5ZIplr1r1_VHuhTE9NBUQZ6WcDBoHiF9vzDwMVMptXPPW6Z6JZRBNxOx6VeGuC7fRBPW8oTROCZd4ZnammiAdGcBzFvmcRuPUNER7sqEfNoxLcOag";
         
            try {
            $client = new GuzzleHttp\Client(['base_uri' => 'https://api.linkedin.com']);
            $response = $client->request('GET', '/v2/me', [
                'headers' => [
                    "Authorization" => "Bearer " . $access_token,
                ],
            ]);
            $data = json_decode($response->getBody()->getContents(), true);
            $linkedin_profile_id = $data['id']; // store this id somewhere
            } catch(Exception $e) {
                echo $e->getMessage();
            }

            $link = $likendenData['url'];
            $access_token = $access_token;
            $linkedin_id = $linkedin_profile_id;
            $body = new \stdClass();
            $body->content = new \stdClass();
            $body->content->contentEntities[0] = new \stdClass();
            $body->text = new \stdClass();
            $body->content->contentEntities[0]->thumbnails[0] = new \stdClass();
            $body->content->contentEntities[0]->entityLocation = $link;
            $body->content->contentEntities[0]->thumbnails[0]->resolvedUrl = $likendenData['image'];
            $body->content->title = $likendenData['title'];
            $body->owner = 'urn:li:person:'.$linkedin_id;
            $body->text->text = $likendenData['subtitle'];
            $body_json = json_encode($body, true);

            try {
                $client = new GuzzleHttp\Client(['base_uri' => 'https://api.linkedin.com']);
                $response = $client->request('POST', '/v2/shares', [
                    'headers' => [
                        "Authorization" => "Bearer " . $access_token,
                        "Content-Type"  => "application/json",
                        "x-li-format"   => "json"
                    ],
                    'body' => $body_json,
                ]);
             
                if ($response->getStatusCode() !== 201) {
                    echo 'Error: '. $response->getLastBody()->errors[0]->message;
                }

                $linkedinDate = date('Y-m-d');
                echo 'Post is shared on LinkedIn successfully.';
            } catch(Exception $e) {
                echo $e->getMessage(). ' for link '. $link;
            }
        echo 'success';exit();  
}

function twitterShare($twitterData){

    $postText = strip_tags(Str::limit($twitterData['title'],100));
    $newTwitte = ['status' => $postText.' '.$twitterData['url']];
    $curl = curl_init();
    $headers = array("Content-type: text/xml");
    curl_setopt_array($curl, array(
    CURLOPT_URL => $twitterData['image'],
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => $headers,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_FOLLOWLOCATION =>1,
    ));
    $results = curl_exec($curl);
    $uploaded_media =Twitter::uploadMedia(['media' => $results]);
    $twitter = Twitter::postTweet($newTwitte);
    Twitter::linkify($twitter);
    echo 'success';exit();
}

function get_fb_app_details(){
    $fb_app_details = Social_media::select('fb_app_id','fb_app_secret')->first();
    return $fb_app_details;
}

function get_fb_user_access_token(){
    $fb_user_access_token = Social_media::value('fb_user_access_token');
    return $fb_user_access_token;
}

function get_fb_page_access_token(){
    $fb_page_access_token = Social_media::value('fb_page_access_token');
    return $fb_page_access_token;
}

function get_social_access_token($social_media_name){
    if($social_media_name=='linkedin'){
        $get_token = Social_media::where('name','=','linkedin')->first();
    }elseif ($social_media_name=='facebook') {
        $get_token = Social_media::where('name','=','facebook')->first();
    }
    return $get_token;
}

?>