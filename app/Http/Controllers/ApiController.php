<?php

namespace App\Http\Controllers;

use App\Models\Collection;
use App\Models\Item;
use App\Models\Language;
use App\Models\LabelTranslation;
use Illuminate\Http\Request;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Database\Eloquent\Builder;

class ApiController extends BaseController 

     public function fetchActiveLang()

        $supportedLang = Language::select('name','alias_name')->where('is_active','active')->get();
        
        $data = [];
        foreach($supportedLang as $key => $value){

            $data[$key][] = $value->alias_name;
            $data[$key][] = $value->name;
        }
        $data1[0][] = '*';
        $data1[0][] = '-all-';


        $newData = array_merge($data,$data1);        
        return json_encode($newData);
    }

    /*public function translateListing(){

        $supportedLang = Language::select('name','alias_name')->where('is_active','active')->get();
        
        $data = [];
        foreach($supportedLang as $key => $value){

            $data[$key]['id'] = $value->alias_name;
            $data[$key]['name'] = $value->name;
        }
        return json_encode($data);
    }

    public function getLabel($id){

        $allLabels = LabelTranslation::where('language_id',$id)->get();
        $data = [];

        foreach($allLabels as $key => $value){

            $data[$key]['label_translate'] = $value->label_translate;
        }
        return json_encode($data);
    }*/


}
