<?php
namespace App\Http\Controllers\Admin;
use Twitter;
use File;
use Facebook;
use App\Models\Item;
use App\Models\ItemContent;
use App\Models\CollectionContent;
use App\Models\Social_media;
use Inertia\Inertia;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Classes\FacebookSdk\MyLaravelPersistentDataHandler;

class ShareController extends Controller
{
    //Added to add functionality of facebook by Cyblance
    public function facebook_login(Request $request){
        $fbtoken=get_social_access_token('facebook');
		$fb = new Facebook\Facebook([
			'app_id'=> '2797063680569641',
			'app_secret'=> 'f5f0ff23cb1492ecf59e6eb31ed5ea5e',
			'default_graph_version' => 'v5.0',
			'persistent_data_handler' => new MyLaravelPersistentDataHandler(),
			]); 
			
			$helper = $fb->getRedirectLoginHelper();
			try {
				$accessToken = $helper->getAccessToken();
			} catch(Facebook\Exception\ResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch(Facebook\Exception\SDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
			$oAuth2Client = $fb->getOAuth2Client();
			$tokenMetadata = $oAuth2Client->debugToken($accessToken);
			$tokenMetadata->validateAppId(env("FACEBOOK_APP_ID"));
			$tokenMetadata->validateExpiration();
			
			if (! $accessToken->isLongLived()) {
				try {
					$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
				} catch (Facebook\Exception\SDKException $e) {
					echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
					exit;
				}
			}
           
			$fb_user_access_token = (string) $accessToken;
			$is_fb_user_token_updated = Social_media::where('name','=','facebook')->update(['user_token' => $fb_user_access_token]);
			
			if($is_fb_user_token_updated){
				try {
					$response = $fb->get(
						'/me/accounts?fields=name,access_token',
						$fb_user_access_token
					);
				} catch(FacebookExceptionsFacebookResponseException $e) {
					echo 'Graph returned an error: ' . $e->getMessage();
					exit;
				} catch(FacebookExceptionsFacebookSDKException $e) {
					echo 'Facebook SDK returned an error: ' . $e->getMessage();
					exit;
				}
				$graphEdge = $response->getGraphEdge();
				foreach ($graphEdge as $graphNode) {
					if($graphNode->getField('name') == 'Eiie'){
						$fb_page_access_token = $graphNode->getField('access_token');
						Social_media::where('name','=','facebook')->update(['token' => $fb_page_access_token]);
					}else{
						continue;
					}
				}
			}
			return Redirect::back()->with(['info' => 'Facebook toke succesfully updated']);
		}
    public function share(Request $request){

        $data=$request->get('myArray');
        // echo "<pre>"; print_r($data); exit;
        if(!array_key_exists('social',$data) || !array_key_exists('language',$data) || !array_key_exists('sech',$data)){
            return Redirect::back()->withErrors(['error' => ' Please Select The (*) Filed ']);
        }else{
            $id=$data['id'];
           
            //echo "<pre>"; print_r($id); exit();
            $sech=$data['sech'];
            $social=$data['social'];
            $lang=$data['language']; 
            if(array_key_exists('sech_date',$data)){
            $sechdate1=$data['sech_date'];
            $originalDate = "2010-03-21";
            $sechdate = date("Y-m-d H:i:s", strtotime($sechdate1));
            }else{
                $sechdate="";
            }
            if($data['type']=='collection'){
                $ItemContentQuery=CollectionContent::where('collection_id', $id)->where('lang','=',$lang);
                $item = $ItemContentQuery->first();
            }else{
                $ItemContentQuery=ItemContent::where('item_id', $id)->where('lang','=',$lang);
                $item = $ItemContentQuery->first();
            }
           
            if(!empty($item)){
                $title=$item->title;
                $subtitle=$item->subtitle;
                $slug=$item->slug;
                $itemUrl=$lang.'/item/'.$id.':'.$slug;
                $url=$request->getSchemeAndHttpHost().'/'.$itemUrl;
                if($subtitle==''){
                        $subtitle=$title;
                    }else{
                        $subtitle=$subtitle;
                    }
                $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";
                $data=array('title'=>$title,'subtitle'=>$subtitle,'url'=>$url,'image'=>$simsrc);
            }else{
                return Redirect::back()->withErrors(['error' => ' This Item is not Translated']);
            }
            
            if($social=='ld'){
                if($sech=='share'){
                    $likendenData=array('title'=>$title,'subtitle'=>$subtitle,'url'=>$url,'image'=>$simsrc);
                    $access_token = linkedin_token();   
                    try {
                        $client = new Client(['base_uri' => 'https://api.linkedin.com']);
                        $response = $client->request('GET', '/v2/me', [
                            'headers' => [
                                "Authorization" => "Bearer " . $access_token,
                            ],
                        ]);
                        $data = json_decode($response->getBody()->getContents(), true);
                        $linkedin_profile_id = $data['id']; // store this id somewhere
                        } catch(Exception $e) {
                            echo $e->getMessage();
                        }
                        $link = $likendenData['url'];
                        $access_token = $access_token;
                        $linkedin_id = $linkedin_profile_id;
                        $body = new \stdClass();
                        $body->content = new \stdClass();
                        $body->content->contentEntities[0] = new \stdClass();
                        $body->text = new \stdClass();
                        $body->content->contentEntities[0]->thumbnails[0] = new \stdClass();
                        $body->content->contentEntities[0]->entityLocation = $link;
                        $body->content->contentEntities[0]->thumbnails[0]->resolvedUrl = $likendenData['image'];
                        $body->content->title = $likendenData['title'];
                        $body->owner = 'urn:li:person:'.$linkedin_id;
                        $body->text->text = $likendenData['subtitle'];
                        $body_json = json_encode($body, true);
                        try {
                            $client = new Client(['base_uri' => 'https://api.linkedin.com']);
                            $response = $client->request('POST', '/v2/shares', [
                                'headers' => [
                                    "Authorization" => "Bearer " . $access_token,
                                    "Content-Type"  => "application/json",
                                    "x-li-format"   => "json"
                                ],
                                'body' => $body_json,
                            ]);
                            if ($response->getStatusCode() !== 201) {
                                echo 'Error: '. $response->getLastBody()->errors[0]->message;
                            }
                            $linkedinDate = date('Y-m-d');
                            $ItemContentQuery->update(['linkeden_share_date' => $linkedinDate]);
                            echo 'Post is shared on LinkedIn successfully.';
                        } catch(Exception $e) {
                            echo $e->getMessage(). ' for link '. $link;
                        }
                    return Redirect::back()->with(['info' => ' Item is shared on LinkedIn successfully']);
                }else{
                    $item=$ItemContentQuery->update(['linkeden_sechulde_date'=>$sechdate]);
                    return Redirect::back()->with(['info' => ' Item is Sechulde on LinkedIn successfully']);
                }     
            }elseif($social=='tw'){
                $twitterData=array('title'=>$title,'subtitle'=>$subtitle,'url'=>$url,'image'=>$simsrc);
                if($sech=='share'){
                    $postText = strip_tags(Str::limit($twitterData['title'],100));
                    $newTwitte = ['status' => $postText.' '.$url];
                    $curl = curl_init();
                    $headers = array("Content-type: text/xml");
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => $simsrc,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTPHEADER => $headers,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_FOLLOWLOCATION =>1,
                    ));
                    $results = curl_exec($curl);
                    $uploaded_media = Twitter::uploadMedia(['media' => $results]);
                    //echo "<pre>"; print_r($uploaded_media); exit();
                    $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                    $configTwitter= Twitter::postTweet($newTwitte); 
                    Twitter::linkify($configTwitter);
                    $twitterDate = date('Y-m-d');
                    $ItemContentQuery->update(['twitter_share_date' => $twitterDate]);
                    return Redirect::back()->with(['info' => ' Post is shared on Twitter successfully']);
                }else{
                    $ItemContentQuery->update(['twitter_shechdule_date'=>$sechdate]);
                    return Redirect::back()->with(['info' => ' Item is Sechulde on Twitter successfully']);
                }    
            }elseif($social=='fb'){
               
                $facebookData=array('title'=>$title,'subtitle'=>$subtitle,'url'=>$url,'image'=>$simsrc);
               
                if($sech=='share'){
                    //echo $facebookData['url']; exit();
                   // echo "in fb if "; exit();
                    $fbtoken=get_social_access_token('facebook');
                    //echo "<pre>"; print_r($fbtoken);
                    if($fbtoken){
                        //echo "<pre>"; print_r($fbtoken->token);
                        //echo "<pre>"; print_r($fbtoken->user_token); exit();
                        $fb = new \Facebook\Facebook([
                            'app_id' => '2797063680569641',
                            'app_secret' => 'f5f0ff23cb1492ecf59e6eb31ed5ea5e',
                            'default_graph_version' => 'v5.0',
                            'persistent_data_handler' => new MyLaravelPersistentDataHandler(),
                          ]);
                          //echo "<pre>"; print_r($fb); exit();
                        try {
                            $response = $fb->get('/debug_token?input_token='.$fbtoken->token,$fbtoken->user_token);
                        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                        echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                        echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;
                        }
                        $graphNode = $response->getGraphNode();  
                        try {
                            $titile = $facebookData['title'];
                            $url=$facebookData['url'];
                            $response = $fb->post(
                                '/me/feed',
                                array(
                                    'message' => $titile,
                                    //'link' => $url,
                                    'published' => '1'
                                ),$fbtoken->token
                            );
                        } catch(FacebookExceptionsFacebookResponseException $e) {
                            $message = 'Graph returned an error: ' . $e->getMessage();
                            return response()->json(['error' => '1', 'message' => $message]);
                        } catch(FacebookExceptionsFacebookSDKException $e) {
                            $message = 'Facebook SDK returned an error: ' . $e->getMessage();
                            return response()->json(['error' => '1', 'message' => $message]);  
                        }
                        $graphNode = $response->getGraphNode();
                        echo $message = 'Post share in facebook successfully.';       
                    }
                    $message = "page access token not set";
                    $facebookDate = date('Y-m-d');
                    $ItemContentQuery->update(['facebook_share_date' => $facebookDate]);
                    return Redirect::back()->with(['info' => ' Post is shared on Facebook successfully']);
                }else{
                    $item=$ItemContentQuery->update(['facebook_shechdule_date'=>$sechdate]);
                    return Redirect::back()->with(['info' => ' Item is Sechulde on Facebook successfully']);
                } 
            }
        }  
    }
}
