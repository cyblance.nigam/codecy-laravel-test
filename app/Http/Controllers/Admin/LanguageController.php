<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use App\Models\Language;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;
use App\Filters\SearchLanguageFilter;

class LanguageController extends Controller
{
    public function index(Request $request)
    {
        
        $languages = QueryBuilder::for(Language::class)
            ->allowedFilters([
                AllowedFilter::custom('search', new SearchLanguageFilter),
            ])
            ->allowedSorts([
                'id',
                'created_at', 
                'updated_at', 
                'name', 
                'alias_name', 
                'is_active',
            ])
            ->defaultSort('name')
            ->withoutGlobalScopes()
            ->paginate(16)
            ;
            
        $result = [
            'languages' => $languages,
            'filter' => $request->all('filter'),
            'sort' => $request->get('sort'),
        ];
        return Inertia::render('Language/List', $result);
    }

    public function edit(Request $request, Language $language) {
    
        $data = [
            'languageModel' => $language,
            'can' => [
                'is_active' => ['update' => 'updateIs_active', $language],
                'language' => ['delete' => 'delete', $language],
            ],
            'is_actives' => ['active' => 'active', 'in-active' => 'in-active'],
        ];
        $sharedCan = Inertia::getShared('can');
        if ($sharedCan) {
            $data['can'] = array_merge_recursive($sharedCan, $data['can']);
        }

        return Inertia::render('Language/Edit', $data);
    }

    public function update(Request $request, Language $language) {

        $request->validate([
            'name' => 'string|filled',
            'is_active' => 'string|filled',
            'alias_name' => 'string|filled',
        ]);

        if ($request->has('name')) {
            $language->name = $request->input('name');
        }
        if ($request->has('alias_name')) {
            $language->alias_name = $request->input('alias_name');
        }
        if ($request->has('is_active')) {

            $language->is_active = $request->input('is_active');
        }
        $language->save();

        return Redirect::route('admin.languages.edit', $language)->with(['info' => 'Language updated']);
    }
}

