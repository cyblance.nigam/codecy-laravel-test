<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Inertia\Inertia;
use App\Models\Collection;
use App\Models\Item;
use App\Models\CollectionContent;
use App\Sorts\CollectionContentTitleSort;
use App\Filters\CollectionsWithImageFilter;
use App\Filters\MissingTranslationsFilter;
use App\Filters\CollectionTreeSearchTitles;
use App\View\Components\RenderContent;
use Illuminate\Support\Facades\DB; 

use App\Actions\Patch;
use App\Actions\CountCollectionsItems;
use App\Actions\ImportItems;
use App\Actions\CollectionCreateDefaults;
use App\Actions\AllowedCollectionsLayouts;
use App\Actions\SaveItemsOrdering;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;
use App\Filters\SearchCollectionFilter;

// Add by Cyblance
use App\Models\Language;

class CollectionController extends Controller
{
    public function index(Request $request)
    {     
        //echo "from item dropdown"; exit();
        $isInertia = $request->input('_format') != 'json';
        $types = explode(',', $request->input('filter.type'));
        $parentTypes = [];
        $childTypes = [];
        if (in_array('dossier', $types) && in_array('dossier_sub', $types)) {
            $parentTypes = ['dossier'];
            $childTypes = ['dossier_sub'];
        }
        if (in_array('sdi_group', $types) && in_array('workarea', $types)) {
            $parentTypes = ['sdi_group'];
            $childTypes = ['workarea'];
        }
        if (in_array('region', $types) && in_array('country', $types)) {
            echo "country"; exit();
            $parentTypes = ['region'];
            $childTypes = ['country'];
        }
        $withTreeCollections = [];

        if (count($parentTypes)) {
            $withTreeCollections = [
                'subCollections' => fn($q) => 
                    $q->withoutGlobalScopes()
                        ->whereIn('type', $childTypes), 
                'subCollections.contents:id,collection_id,title,lang', 
                'parentCollections' => fn($q) => 
                    $q->withoutGlobalScopes()
                        ->whereIn('type', $parentTypes), 
                'parentCollections.contents:id,collection_id,title,lang',
            ];
        } else {
            $withTreeCollections = [
                'subCollections' => fn($q) => $q->withoutGlobalScopes(), 
                'subCollections.contents:id,collection_id,title,lang', 
                'parentCollections' => fn($q) => $q->withoutGlobalScopes(), 
                'parentCollections.contents:id,collection_id,title,lang',
            ];
        }
        
        $titleSort = AllowedSort::custom(
            'title', 
            new CollectionContentTitleSort($parentTypes, $childTypes), 
            'title'
        );
        $defaultSort = count($parentTypes) ? $titleSort : '-created_at';

        $collections = QueryBuilder::for(Collection::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('type'),
                AllowedFilter::exact('status'),
                AllowedFilter::exact('layout'),
                // AllowedFilter::partial('search', 'contents.title'),
                AllowedFilter::custom('check', new SearchCollectionFilter),
                AllowedFilter::custom(
                    'search', 
                    new CollectionTreeSearchTitles($parentTypes, $childTypes)
                ),
                AllowedFilter::custom('imageitem.id', new CollectionsWithImageFilter),
                AllowedFilter::custom('missing_translations', new MissingTranslationsFilter),
                AllowedFilter::callback('items_count', fn($q, $val) => $q->having('items_count', $val)),
            ])
            ->allowedSorts([
                'id',
                'created_at', 
                'updated_at', 
                'type', 
                'layout', 
                'status',
                $titleSort,
                AllowedSort::field('items_count', 'items_count')
            ])
            ->defaultSort($defaultSort)
            ->with([
                'contents:id,collection_id,title,lang'
            ])
            ->with(
                $withTreeCollections
            )
            ->without([
                'content',
                'subCollections.content',
                'parentCollections.content',
            ])
            ->withCount(['items'])
            ->withoutGlobalScopes()            
            ;
        
        if ($request->input('filter.status') == 'published' && count($parentTypes))
        {
            $collections = $collections->whereHas(
                'parentCollections', 
                fn($q) => $q->where('status', 'published')
            );
        }
        if(isset($request->trash) && $request->trash=='yes'){
            $collections = $collections->onlyTrashed();
        }else{
            $collections = $collections->where('deleted_at',null);
        }
    
        if ($isInertia) {
            $collections = $collections->paginate(16)->appends(request()->query());
        } else {
            $collections = $collections->jsonPaginate()->appends(request()->query());
        }

        // Translation Added BY Cyblance start
        
        $supportedLang = translate_support_language();
        $languageTranslateForSelect = Language::select('languages.name','languages.alias_name')
        ->where('languages.is_active','active')
        ->get();

        $getActiveLanguages = [];
        foreach($languageTranslateForSelect as $key => $value){
            $getActiveLanguages[$value->alias_name] =  $value->name;
        }
        // Translation Added BY Cyblance end

        $result = [
            'collections' => $collections,
            'filter' => $request->all('filter'),
            'sort' => $request->input('sort'),
            'parent_types' =>  $parentTypes,
            'child_types' => $childTypes,
            'supportedLang' => $supportedLang,
            'isActiveLanguages' => $getActiveLanguages,
        ];

        if (!$isInertia) {
            return response()->json($result);
        }
        return Inertia::render('Collections/List', $result);
    }

    public function edit(Request $request, AllowedCollectionsLayouts $allowedAction, $id) {
        $collection = Collection::where('id', $id)
            ->with([
                'contents', 
                'allImages', 
                'allImages.contents', 
                'allImages.contents.images',
                'slotItems', 
                // 'subCollections', 
                'subCollections' => function($q) { $q->withoutGlobalScopes(); }, 
                'subCollections.contents', 
                // 'parentCollections',
                'parentCollections' => function($q) { $q->withoutGlobalScopes(); }, 
                'parentCollections.contents'
            ])
            ->withCount(['items'])
            ->withoutGlobalScopes()
            ->first()
            ;
        
        if ($collection->ordering === 'partial_date') {

        }

        $data = [
            'collection' => $collection ,
            'id' => $id,
            'items' => Inertia::lazy(function() use ($collection) { 
                $items = $collection->items()
                    ->withoutGlobalScopes()
                    ->withPivot(['item_order'])
                    ->with(['contents:item_id,id,lang,title'])
                    ->orderBy('item_order')
                    ;
                if ($collection->ordering === 'partial_date') {
                    $items = $items->where('item_order', '<', 9999999);
                }
                return $items->get();
            }),
        ];

        $canCreate = $request->user()->can('create', Collection::class);
        $canCreateLimited = $request->user()->can('createLimited', Collection::class);
        $allowed = $allowedAction->execute($canCreateLimited, $canCreate);
        if (isset($allowed['layouts'][$collection->type])) {
            $data['layouts'] = $allowed['layouts'][$collection->type];
        }

        if (false && $collection->slots_template_id) {
            $data['slots_template'] = \App\Models\CollectionSlot::where('template_id', $collection->slots_template_id)
                ->orderBy('order')
                ->with(['title'])
                ->get()
                ;
        }

        return Inertia::render('Collections/Edit', $data);
    }

    public function update(Request $request, Patch $patchAction, $id) {
        $collection = Collection::withoutGlobalScopes()->findOrFail($id);
        if ($request->user()->cannot('update', $collection)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
        // check collection associations

        // patching according to JSON diff delta format
        // https://github.com/benjamine/jsondiffpatch/blob/master/docs/deltas.md
        //Added code for check the validation of title by Cyblance 
        
        //Added by Cyblance for title validation
        $forContent = CollectionContent::select('title')->where('collection_id', $id)->get();
        $content_check=$request->all();
        if(array_key_exists("contents",$content_check)){
            foreach($request->contents as $allContent){
                $info = array();
                if (is_array($allContent)){
                    if(isset($allContent[ 'title' ])){
                        $info = array($allContent[ 'title' ]);
                        if($info[0][1]==null){
                            return Redirect::back()->withErrors(['error' => 'Please Enter Title']);
                        }
                    }else{ 
                        if($allContent[0]['title']==''){
                            return Redirect::back()->withErrors(['error' => 'Please Enter Title']);
                        }
                    }
                }
            }  
        }
        
        $patchAction->execute($collection, $request->all());
        $collection->refresh();
        foreach ($collection->contents as $content) {
            $content->slug = Str::slug($content->title ? $content->title : '-');
            $renderBlurb = new RenderContent($content, 'html', null, null, true);
            $content->blurb = $renderBlurb->output;
        }
        $collection->push();
        //Added code separated success messege for each type of Collection by Cyblance 
        $succesMessage=item_success_message($collection->type);
        return Redirect::route('admin.collections.edit', $collection)
                        ->with(['info' => $succesMessage.' updated']);
    }

    public function create(Request $request, AllowedCollectionsLayouts $allowedAction) {
        $canCreate = $request->user()->can('create', Collection::class);
        $canCreateLimited = $request->user()->can('createLimited', Collection::class);
        if (!$canCreate && !$canCreateLimited) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }

        $data = $allowedAction->execute(
            $canCreateLimited, 
            $canCreate, 
            $request->input('parent_collection_id')
        );
        
        $collection = false;
        if ($request->has('parent_collection_id')) {
            $collection = Collection::where('id', $request->input('parent_collection_id'))
                ->withoutGlobalScopes()
                ->with(['contents:id,lang,title'])
                ->first()
                ;
            $data['parentCollection'] = $collection;
        }

        return Inertia::render('Collections/Create', $data);
    }

    public function store(Request $request, CollectionCreateDefaults $createDefaultsAction) {
        $canCreate = $request->user()->can('create', Collection::class);
        $canCreateLimited = $request->user()->can('createLimited', Collection::class);
        if (!$canCreate && !$canCreateLimited) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }

        $create = $request->only(['type', 'layout']);
        $create['status'] = 'draft';
        $collection = Collection::create($create);
        $createDefaultsAction->execute($collection);

        if ($parentCollectionId = $request->input('parent_collection_id')) {
            $collection->parentCollections()->attach(
                $parentCollectionId, 
                ['parent_order' => 0, 'sub_order' => 9999]
            );
        }
        //Added code separated success messege for each type of Collection by Cyblance 
        $succesMessage=item_success_message($request->type);
        
        return Redirect::route('admin.collections.edit', $collection)
                       ->with(['info' => $succesMessage.' created']);
    }

    public function itemsOrdering(Request $request, SaveItemsOrdering $saveItemsOrdering, $id) {
        $collection = Collection::withoutGlobalScopes()->findOrFail($id);
        if ($request->user()->cannot('update', $collection)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
        
        $ids = explode(',', $request->input('item_ids'));
        $unorderIds = explode(',', $request->input('unordered_item_ids'));        
        $saveItemsOrdering->execute($collection, $ids, $unorderIds);

        return response()->json(['success' => true]);
    }

    public function itemsCount(
                Request $request, 
                CountCollectionsItems $countCollectionsItems, 
                $ids
    ) {
        if (!$ids) {
            return response()->json(['count' => 0]);
        }
        $ids = explode(',', $ids);
        $result = $countCollectionsItems->execute(
            $ids, 
            $request->input('existing_collection_id')
        );
        return response()->json($result);
    }

    //Added By Cyblance for delete functionality
    public function delCollection(Request $request, collection $collection) {

        $getCollectionType = Collection::withoutGlobalScopes()->select('type')->where('id', $request->collection)->first();
        CollectionContent::where('collection_id', $request->collection)
                ->update(array('deleted_at' => date('Y-m-d H:i:s')));
        Collection::withoutGlobalScopes()->where('id', $request->collection)->update(array('deleted_at' => date('Y-m-d H:i:s')));
        
        //Added code separated success messege for each type of Collection by Cyblance
        $succesMessage=item_success_message($getCollectionType->type);

        $result = [
            'filter[type]' => $getCollectionType->type,
        ];
        return Redirect::route('admin.collections.index',$result)->with(['info' => $succesMessage .' deleted']);
    }

    public function itemsImport(
            Request $request, 
            ImportItems $importItems, 
            $id
    ) {
        $request->validate([
            'collection_id' => 'required|array',
            'collection_id.*' => 'integer'
        ]);
        
        $collection = Collection::withoutGlobalScopes()->findOrFail($id);
      
        $ids = $request->input('collection_id');
        //echo "<pre>"; print_r($importItems); exit();
        $result = $importItems->execute($collection, $ids);
    
        return Redirect::route('admin.collections.edit', $collection)
                        ->with(['info' => 'Items imported']);
    }

    //Added By Cyblance for delete functionality
    public function bulkDelete(Request $request){

        $getAllData = $request->all();
        $getItemType = Collection::withoutGlobalScopes()->select('type')->where('id', $getAllData['getResult'])->first();
        $collectionQuery = Collection::withoutGlobalScopes()->whereIn('id', $getAllData['getResult']);
        $contentQuery = CollectionContent::whereIn('collection_id',$getAllData['getResult']);
        $getCollectionID = $contentQuery->get();
        if($getAllData['getResultAction']=='trashed' || $getAllData['getResultAction']=='restored'){
            if($getAllData['getResultAction']=='trashed'){
                $action = date('Y-m-d H:i:s');
            }else{
                $action = NULL;
            }
            if(!$getCollectionID->isEmpty()){
                $contentQuery->update(array('deleted_at' => $action));  
            }
            $collectionQuery->update(array('deleted_at' => $action));
        }else{
            if(!$getCollectionID->isEmpty()){
                $contentQuery->forceDelete(); 
            }
            $collectionQuery->forceDelete();
        }
    
        //Added code separated success messege for each type of Collection by Cyblance 
        $succesMessage = item_success_message($getItemType->type);
        return Redirect::back()->with(['info' =>  'Selected '. $succesMessage .' are '. $getAllData['getResultAction']]);
    }
    // Trnaslation functionlity in collection
    public function translateCollection($lang,$ids){
        $collectionForEn = CollectionContent::where('collection_id',$ids)->where('lang','en')->first();
        if(!$collectionForEn){
            $collectionForEn = CollectionContent::where('collection_id',$ids)->where('lang','*')->first();
        }
        $translate_title[] = $collectionForEn->title;
        $translate_subtitle[] = $collectionForEn->subtitle;
        $translate_slug[] = $collectionForEn->slug;
        $translate_blurb[] = $collectionForEn->blurb;
        if($collectionForEn->title!=''){
            $translated_title = active_language_translate_tag($lang,$translate_title);  
            $title = $translated_title[0];
        }else{
            $title = $collectionForEn->title;
        }

        if($collectionForEn->subtitle!=''){
            $translated_subtitle = active_language_translate_tag($lang,$translate_subtitle);    
            $subtitle = $translated_subtitle[0];
        }else{
            $subtitle = $collectionForEn->subtitle;
        }

        if($collectionForEn->slug!=''){
            $translated_slug = active_language_translate_tag($lang,$translate_slug);    
            $slug = $translated_slug[0];
        }else{
            $slug = $collectionForEn->slug;
        }

        if($collectionForEn->blurb!=''){
            $translated_blurb = active_language_translate_tag($lang,$translate_blurb);  
            $blurb = $translated_blurb[0];
        }else{
            $blurb = $collectionForEn->blurb;
        }

        $collectionContent = new CollectionContent();
        $collectionContent->collection_id = $ids;
        $collectionContent->title = $title;
        $collectionContent->subtitle = $subtitle;
        $collectionContent->blurb = $blurb;
        $collectionContent->lang = $lang;
        $collectionContent->slug = $slug;
        $collectionContent->save();
        return Redirect::back()->with(['info' => 'Collection Translated Successfully']);
    }
}

