<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

use App\Models\Item;
use App\Models\Collection;
use App\Models\ItemContent;
use App\Filters\CollectionMemberFilter;
use App\Filters\ItemsWithImageFilter;
use App\Filters\MissingTranslationsFilter;
use App\Sorts\ItemContentTitleSort;
use App\View\Components\RenderContent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;
use App\Actions\Patch;
use App\Actions\CleanHtml;
use App\Filters\MissingFilesFilter;

//Added by Cyblance search by id in listing
use App\Filters\SearchItemFilter;
use App\Models\Language;
use App\Models\Contact;
use App\Models\DCProject;

class ItemController extends Controller
{
    public function index(Request $request){
    
        $isInertia = $request->input('_format') != 'json';

        $items = QueryBuilder::for(Item::class)
            ->allowedFilters([
                AllowedFilter::exact('type'),
                AllowedFilter::exact('subtype'),
                AllowedFilter::exact('status'),
                //Added by Cyblance search by id in listing in item
                AllowedFilter::custom('search', new SearchItemFilter),
                AllowedFilter::custom('collection.id', new CollectionMemberFilter),
                AllowedFilter::custom('imageitem.id', new ItemsWithImageFilter),
                AllowedFilter::custom('missing_translations', new MissingTranslationsFilter),
                AllowedFilter::custom('missing_files', new MissingFilesFilter),
                AllowedFilter::callback(
                    'missing_workarea', 
                    fn($q) => $q->whereDoesntHave(
                        'collections', 
                        fn($q) => $q->where('type', 'workarea')
                    )
                )
            ])
            ->allowedSorts([
                'id',
                'created_at', 
                'updated_at', 
                'type', 
                'layout', 
                AllowedSort::custom('title', new ItemContentTitleSort, 'title')
                // AllowedSort::field('items_count', 'items_count')
            ])
            ->defaultSort('-created_at')
            ->with([
                'contents:id,item_id,title,lang'
            ])
            ->withoutGlobalScopes();

        //Added By Cyblance for delete functionality
        if(isset($request->trash) && $request->trash=='yes'){
            $items = $items->onlyTrashed();
        }else{
            $items = $items->where('deleted_at',null);
        }
        if ($isInertia) {
            $items = $items->paginate(16)->appends(request()->query());
        } else {
            $items = $items->jsonPaginate()->appends(request()->query());
        }
         
        // Translation Added BY Cyblance start
        
        $supportedLang = translate_support_language();
        $languageTranslateForSelect = Language::select('languages.name','languages.alias_name')
        ->where('languages.is_active','active')
        ->get();

        $getActiveLanguages = [];
        foreach($languageTranslateForSelect as $key => $value){
            $getActiveLanguages[$value->alias_name] =  $value->name;
        }
        // Translation Added BY Cyblance end
        
        $result = [
            'user' => $request->user(),
            'items' => $items,
            'filter' => $request->all('filter'),
            'supportedLang' => $supportedLang,
            'isActiveLanguages' => $getActiveLanguages,
            'sort' => $request->get('sort'),
        ];
        if (isset($result['filter']['filter']['collection.id'])) {
            $collectionId = $result['filter']['filter']['collection.id'];
            $result['collection'] = Collection::where('id', $collectionId)
                    ->withoutGlobalScopes()
                    ->with([
                        'contents:id,lang,title'
                    ])->first();
        }

        if (!$isInertia) {
            return response()->json($result);
        }
        return Inertia::render('Items/List', $result);
    }


    public function edit(Request $request, CleanHtml $cleanHtmlAction, $id) {
        $item = Item::where('id', $id)
            ->with([
                'contents', 
                'contents.images', 
                'contents.videos', 
                'contents.files', 
                'contents.links', 
                'contents.contacts', 
                'contents.dcprojects' => fn($q) => $q->withoutGlobalScopes(),
                'contents.dcprojects.hostOrganisations',
                'contents.dcprojects.hostOrganisations.content',
                'contents.dcprojects.hostOrganisations.affiliate:item_id,acronym,official_name',
                'contents.dcprojects.cooperatingOrganisations',
                'contents.dcprojects.cooperatingOrganisations.content',
                'contents.dcprojects.cooperatingOrganisations.affiliate:item_id,acronym,official_name',
                'allImages' => fn($q) => $q->withoutGlobalScopes(), 
                'allImages.contents', 
                'allImages.contents.images',
                'collections' => fn($q) => $q->withoutGlobalScopes(), 
                'collections.contents:id,collection_id,title,lang',
                'collections.parentCollections' => fn($q) => $q->withoutGlobalScopes(),
                'collections.parentCollections.content:id,collection_id,title,lang',
                'attachmentGroups',
                'attachmentGroups.contents' => fn($q) => $q->withoutGlobalScopes(),
                'attachmentGroups.attachments',
                'attachmentGroups.attachments.item' 
                        => fn($q) => $q->withoutGlobalScopes()->without('content'),
                'attachmentGroups.attachments.item.contents:id,item_id,title,lang',
                'attachmentGroups.attachments.item.contents.links',
                'attachmentGroups.attachments.item.contents.videos',
                'attachmentGroups.attachments.item.contents.files',
            ])
            ->withCount([
                'imageForItems',
                'imageForCollections'
            ])
            ->without([
                'collections.slotItems',
                'contents.updated_at',
                'contents.created_at',
            ])
            ->withoutGlobalScopes()
            ->first()
            ;

        // $cleanHtmlAction->execute($item);

        $audits = [
            'main' => $item->audits()->with('user')->latest()->take(10)->get(),
            'contents' => $item->contents->map(fn($content) => ([
                'lang' => $content->lang,
                'audits' => $content->audits()->with('user')->latest()->take(10)->get()
            ]))
        ];


        $data = [
            'item' => $item,
            'audits' => $audits,
            'id' => $id
        ];

        return Inertia::render('Items/Edit', $data);
    }

    public function create(Request $request) {
        if ($request->user()->cannot('create', Item::class)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }

        $collection = false;
        if ($request->has('collection_id')) {
            $collection = Collection::where('id', $request->input('collection_id'))
                ->withoutGlobalScopes()
                ->with(['contents:id,lang,title'])
                ->first()
                ;
        }

        $result = [
            'types' => [
                'article' => 'Article', 
                'static' => 'Page', 
                'resource' => 'Resource', 
                'contact' => 'Contact card', 
                'person' => 'Person', 
                'library' => 'Library', 
                // 'slot' => 'Collection slot',
                'dcproject' => 'DC Project'
            ],
            'subtypes' => [
                'resource' => [
                    'file' => 'File', 
                    'image' => 'Image', 
                    'image.icon' => 'Icon Image', 
                    'image.square' => 'Square Image', 
                    'image.portrait' => 'Portrait Image', 
                    'video' => 'Video', 
                    'link' => 'External Link'
                ]
            ],           
            'collection_prepick' => [
                'default' => [
                    [
                        'label' => 'Priorities', 
                        'filter' => ['type' => 'sdi_group,workarea'],
                        'onlyChildTypes' => true,
                        'groupByParent' => true,
                    ],
                    [
                        'label' => 'Dossiers', 
                        'filter' => ['type' => 'dossier,dossier_sub'],
                        'onlyChildTypes' => true,
                        'groupByParent' => true,
                    ],
                    ['label' => 'Regions', 'filter' => ['type' => 'region']],
                    ['label' => 'Countries', 'filter' => ['type' => 'country']],
                ],
                'article' => [
                    [
                        'label' => 'Latest', 
                        'filter' => [
                            'id' => implode(',', [
                                config('eiie.collection.news'),
                                config('eiie.collection.opinion'),
                                config('eiie.collection.take-action'),
                                config('eiie.collection.statements'),
                            ])
                        ],
                        'mode' => 'radio',
                    ],
                ],
                'resource' => [
                    [
                        'label' => 'Libraries',
                        'filter' => ['type' => 'library'],
                    ]
                ],
            ]
        ];

        // foreach ($result['regions'] as $region) {
        //     $result['region_'.$region->id] = Inertia::lazy(fn() => $region->subCollections);
        // }
        // foreach ($result['dossiers'] as $dossier) {
        //     $result['dossier_'.$dossier->id] = Inertia::lazy(fn() => $dossier->subCollections);
        // }

        if ($collection) {
            $result['collection'] = $collection;

            if ($collection->type == 'contacts') {
                $result['types'] = ['contact' => 'Contact'];
            }
            if ($collection->type == 'library') {
                $result['types'] = ['library' => 'Library'];
            }
            if ($collection->type == 'persons') {
                $result['types'] = ['person' => 'Person'];
            }
        }

        return Inertia::render('Items/Create', $result);
    }

    public function store(Request $request) {
        if ($request->user()->cannot('create', Item::class)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }

        $request->validate([
            'type' => 'required',
            'subtype' => '',
            'collection_id' => 'integer',
            'collection_ids' => 'array',
            'collection_ids.*' => 'integer',
            'languages' => 'array',
            'languages.*' => 'string',
        ]);

        $create = $request->only(['type', 'subtype']);
        $create['status'] = 'draft';
        $item = Item::create($create);
        $languages = $request->input('languages');
        if (empty($languages)) {
            $languages[0] = 'en';
            if ($item->type == 'resource' || $item->type == 'contact' || $item->type == 'person') {
                $languages[0] = '*';
            }
        }
        foreach ($languages as $lang) {
            $content = $item->contents()->create(['lang' => $lang]);
            switch($request->input('type')) {
                case 'contact':
                case 'person':
                    $content->contacts()->create();
                    break;
                case 'dcproject':
                    $content->dcProjects()->create();
                    break;
                case 'resource':
                    switch ($request->input('subtype')) {
                        case 'video':
                            $content->videos()->create(['provider' => 'youtube', 'provider_id' => '']);
                            break;
                        case 'link':
                            $content->links()->create(['url' => '']);
                            break;
                        }
            }
        }
        if ($collectionId = $request->input('collection_id')) {
            $item->collections()->attach($collectionId);
        }
        if ($collectionIds = $request->input('collection_ids')) {
            $item->collections()->attach($collectionIds);
        }
        
        //Added code separated success messege for each type of Item by Cyblance 
        $succesMessage=item_success_message($request->input('type'),$request->input('subtype'));
        
        return Redirect::route('admin.items.edit', $item)->with(['info' => $succesMessage.' Created']);
    }


    public function update(
            Request $request, 
            Patch $patchAction,
            // UpdateItemContents $contentsAction, 
            // UpdateItemImages $imagesAction, 
            // UpdateAttachmentGroups $attachmentsAction,
            // AssociateItemCollections $collectionsAction,
            $id
        ) {
       // echo "<pre>"; print_r($request->all()); exit();

        $item = Item::withoutGlobalScopes()->findOrFail($id);
        if ($request->user()->cannot('update', $item)) {
            return Redirect::back()->withErrors(['error' => 'Not authorized']);
        }
        
        //Added by Cyblance for title validation
        $forContent =ItemContent::select('title')->where('item_id', $id)->get();
        $content_check=$request->all();
        if(array_key_exists("contents",$content_check)){
            foreach($request->contents as $allContent){
                $info = array();
                if (is_array($allContent)){
                    if(isset($allContent[ 'title' ])){
                        $info = array($allContent[ 'title' ]);
                        if($info[0][1]==null){
                            return Redirect::back()->withErrors(['error' => 'Please Enter Title']);
                        }
                    }else{
                        foreach($forContent as $fContent){
                            if($fContent->title==''){
                                return Redirect::back()->withErrors(['error' => 'Please Enter Title']);
                            }
                        }
                    }
                }
            }  
        }

          


        // patching according to JSON diff delta format
        // https://github.com/benjamine/jsondiffpatch/blob/master/docs/deltas.md

        $patchAction->execute($item, $request->all());
        $item->refresh();                
        foreach ($item->contents as $content) {
            $content->slug = Str::slug($content->title ? $content->title : '-');
            $renderBlurb = new RenderContent($content, 'html', null, null, true);
            $renderContent = new RenderContent($content, 'html', null, null, false, true);
            $content->blurb = $renderBlurb->output;
            $content->content = $renderContent->output;
        }
        $item->push();
        // $item->update($request->only(['status', 'publish_at']));

        // $contentsAction->execute($item, $request->contents);
        // $imagesAction->execute($item, $request->all_images);
        // $attachmentsAction->execute($item, $request->attachment_groups);
        // $collectionsAction->execute($item, $request->collections);
        
        //Added code separated success messege for each type of Item by Cyblance
        $succesMessage=item_success_message($item->type,$item->subtype);

        return Redirect::route('admin.items.edit', $item)->with(['info' => $succesMessage.' updated']);
    }

    //Added By Cyblance for delete functionality
    public function delItem(Request $request, Item $item) {
      
        $getItemType = Item::withoutGlobalScopes()->select('type','subtype')->where('id', $request->item)->first();
        ItemContent::where('item_id', $request->item)
        ->update(array('deleted_at' => date('Y-m-d H:i:s')));
        Item::withoutGlobalScopes()->where('id', $request->item)
        ->update(array('deleted_at' => date('Y-m-d H:i:s')));

        //Added code separated success messege for each type of Item by Cyblance
        $succesMessage=item_success_message($getItemType->type,$getItemType->subtype);
        $result = [
            'filter[type]' => $getItemType->type,
        ];
        return Redirect::route('admin.items.index',$result)->with(['info' => $succesMessage.' Deleted']);
    }

    //Added By Cyblance for delete functionality
    public function bulkDelete(Request $request){
        $getAllData=$request->all();
        $itemQuery=Item::withoutGlobalScopes()->whereIn('id', $getAllData['getResult']);
        $ItemContentQuery=ItemContent::whereIn('item_id', $getAllData['getResult']);

        $getCollectionType = Item::withoutGlobalScopes()->select('type','subtype')->where('id', $getAllData['getResult'])->first();
        $getItemID=$ItemContentQuery->get();
        if($getAllData['getResultAction']=='trashed' || $getAllData['getResultAction']=='restored'){
            if($getAllData['getResultAction']=='trashed'){
                $action = date('Y-m-d H:i:s');
            }else{
                $action = NULL;
            }
            if(!$getItemID->isEmpty()){
                $ItemContentQuery->update(array('deleted_at' => $action));  
            }
            $itemQuery->update(array('deleted_at' => $action));
        }else{
            if(!$getItemID->isEmpty()){
                $ItemContentQuery->forceDelete();  
            }
            $itemQuery->forceDelete();
        }

        //Added code separated success messege for each type of Item by Cyblance
        $succesMessage=item_success_message($getCollectionType->type,$getCollectionType->subtype);
        return Redirect::back()->with(['info' => $succesMessage. ' are '.$getAllData['getResultAction']]);
    }

    // Item Translation Added by cyblance
    public function translate($lang,$ids){
        $itemForEn = ItemContent::where('item_id',$ids)->where('lang','en')->first();
        if(!$itemForEn){
            $itemForEn = ItemContent::where('item_id',$ids)->where('lang','*')->first();
        }
        $translate_title[] = $itemForEn->title;
        $translate_subtitle[] = $itemForEn->subtitle;
        $translate_blurb[] = $itemForEn->blurb;
        $translate_content[] = $itemForEn->content;
        $translate_link[] = $itemForEn->link;  
        if($itemForEn->title!=''){
            $translated_title = active_language_translate_tag($lang,$translate_title);  
            $title = $translated_title[0];
        }else{
            $title = $itemForEn->title;
        }

        if($itemForEn->subtitle!=''){
            $translated_subtitle = active_language_translate_tag($lang,$translate_subtitle);    
            $subtitle = $translated_subtitle[0];
        }else{
            $subtitle = $itemForEn->subtitle;
        }

        if($itemForEn->blurb!=''){
            $translated_blurb = active_language_translate_tag($lang,$translate_blurb);  
            $blurb = $translated_blurb[0];
        }else{
            $blurb = $itemForEn->blurb;
        }

        if($itemForEn->content!=''){
            $translated_content = active_language_translate_tag($lang,$translate_content);  
            $content = $translated_content[0];
        }else{
            $content = $itemForEn->content;
        }

        if($itemForEn->link!=''){
            $translated_link = active_language_translate_tag($lang,$translate_link);    
            $link = $translated_link[0];
        }else{
            $link = $itemForEn->link;
        }
        
        $itemContent = new ItemContent();
        $itemContent->item_id = $ids;
        $itemContent->title = $title;
        $itemContent->subtitle = $subtitle;
        $itemContent->blurb = $blurb;
        $itemContent->content = $content;
        $itemContent->lang = $lang;
        //$itemContent->link = $link;
        $itemContent->save();
        /*$itemContent = ItemContent::create([
            'item_id' => $ids,
            'title' => $title,
            'subtitle' => $subtitle,
            'blurb' => $blurb,
            'content' => $content,
            'lang' => $lang,
            'link' => $link
            ]);*/
        $itemContact = Contact::where('item_content_id',$itemForEn->id)->first();
        if($itemContact){
            /*$content = new Contact();
            $content->item_content_id = $itemContent->id;
            $content->save();*/
            $content = Contact::create(['item_content_id' => $itemContent->id]);
        }
        if($itemForEn){
            $dcproject = DCProject::where('content_id',$itemForEn->id)->first();
            if($dcproject){
                $translate_contact_person_name[] = $dcproject->contact_person_name;
                $translate_description[] = $dcproject->description;
                $translate_goals[] = $dcproject->goals;
                $translate_activity_type[] = $dcproject->activity_type;
                $translate_results[] = $dcproject->results;
                $translate_funding[] = $dcproject->funding;
                $translate_budget[] = $dcproject->budget;
                $translate_url[] = $dcproject->url;
                $translate_host_orgs_str[] = $dcproject->host_orgs_str;
                $translate_coop_orgs_str[] = $dcproject->coop_orgs_str;
                $translate_countries_str[] = $dcproject->countries_str;
                $translate_topics_str[] = $dcproject->topics_str;
                if($dcproject->contact_person_name!=''){
                    $translated_contact_person_name = active_language_translate_tag($lang,$translate_contact_person_name);  
                    $contact_person_name = $translated_contact_person_name[0];
                }else{
                    $contact_person_name = $dcproject->contact_person_name;
                }

                if($dcproject->description!=''){
                    $translated_description = active_language_translate_tag($lang,$translate_description);  
                    $description = $translated_description[0];
                }else{
                    $description = $dcproject->description;
                }

                if($dcproject->goals!=''){
                    $translated_goals = active_language_translate_tag($lang,$translate_goals);  
                    $goals = $translated_goals[0];
                }else{
                    $goals = $dcproject->goals;
                }

                if($dcproject->activity_type!=''){
                    $translated_activity_type = active_language_translate_tag($lang,$translate_activity_type);  
                    $activity_type = $translated_activity_type[0];
                }else{
                    $activity_type = $dcproject->activity_type;
                }

                if($dcproject->results!=''){
                    $translated_results = active_language_translate_tag($lang,$translate_results);  
                    $results = $translated_results[0];
                }else{
                    $results = $dcproject->results;
                }

                if($dcproject->host_orgs_str!=''){
                    $translated_host_orgs_str = active_language_translate_tag($lang,$translate_host_orgs_str);  
                    $host_orgs_str = $translated_host_orgs_str[0];
                }else{
                    $host_orgs_str = $dcproject->host_orgs_str;
                }

                if($dcproject->coop_orgs_str!=''){
                    $translated_coop_orgs_str = active_language_translate_tag($lang,$translate_coop_orgs_str);  
                    $coop_orgs_str = $translated_coop_orgs_str[0];
                }else{
                    $coop_orgs_str = $dcproject->coop_orgs_str;
                }

                if($dcproject->countries_str!=''){
                    $translated_countries_str = active_language_translate_tag($lang,$translate_countries_str);  
                    $countries_str = $translated_countries_str[0];
                }else{
                    $countries_str = $dcproject->countries_str;
                }

                if($dcproject->topics_str!=''){
                    $translated_topics_str = active_language_translate_tag($lang,$translate_topics_str);    
                    $topics_str = $translated_topics_str[0];
                }else{
                    $topics_str = $dcproject->topics_str;
                }
                /*$insertDCProject = new DCProject();
                $insertDCProject->content_id = $itemContent->id;
                $insertDCProject->contact_person_name = $contact_person_name;
                $insertDCProject->description = $description;
                $insertDCProject->goals = $goals;
                $insertDCProject->activity_type = $activity_type;
                $insertDCProject->results = $results;
                $insertDCProject->host_orgs_str = $host_orgs_str;
                $insertDCProject->coop_orgs_str = $coop_orgs_str;
                $insertDCProject->countries_str = $countries_str;
                $insertDCProject->topics_str = $topics_str;
                $insertDCProject->save();*/
                $insertDCProject = DCProject::create(['content_id' => $itemContent->id,
                                             'contact_person_name' => $contact_person_name,
                                             'description' => $description,
                                             'goals' => $goals,
                                             'activity_type' => $activity_type,
                                             'results' => $results,
                                             'host_orgs_str' => $host_orgs_str,
                                             'coop_orgs_str' => $coop_orgs_str,
                                             'countries_str' => $countries_str,
                                             'topics_str' => $topics_str]);
            }
        }
        return Redirect::back()->with(['info' => 'Item Translated Successfully']);
    }

}

