<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Item;
use App\Models\ItemContent;
use App\Filters\CollectionMemberFilter;
use App\Filters\SearchContentsAndResourcesFilter;
use App\Sorts\ItemContentTitleSort;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\Filters\Filter;
use App\Actions\CreateResourceItem;

//Added by Cyblance search by id in listing
use App\Filters\SearchTitleFilter;
// Added by Cybalnce For azure Image Upload
use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Blob\Models\CreateBlockBlobOptions;

class ResourceItemController extends Controller
{
    public function index(Request $request) {
        $items = QueryBuilder::for(Item::class)
            ->allowedFilters([
                AllowedFilter::exact('type'),
                AllowedFilter::exact('subtype'),
                //Added by Cyblance search by id in listing
                AllowedFilter::custom('search', new SearchTitleFilter),
                //AllowedFilter::custom('search', new SearchContentsAndResourcesFilter),
                // AllowedFilter::exact('collection.id', null, false)
                AllowedFilter::custom('collection.id', new CollectionMemberFilter)
            ])
            ->allowedSorts([
                'id',
                'created_at', 
                'updated_at', 
                'type', 
                'layout', 
                AllowedSort::custom('title', new ItemContentTitleSort, 'title')
                // AllowedSort::field('items_count', 'items_count')
            ])
            ->defaultSort('-created_at')
            ->with([
                'contents:id,item_id,title,lang,slug',
                'contents.images',
                'contents.links',
                'contents.videos',
                'contents.files',
            ])
            ->withoutGlobalScopes()
            // ->withCount(['items'])
            ->jsonPaginate()
            ->appends(request()->query())
            ;
        
        

        return response()->json(['resourceitems' => $items]);
    }

    public function show(Request $request, $id) {
        $item = Item::where('id', $id)
                    ->with([
                        'contents:id,item_id,title,lang',
                        'contents.images',
                        'contents.links',
                        'contents.videos',
                        'contents.files',
                    ])
                    ->withoutGlobalScopes()
                    ->first()
                    ;
        return response()->json($item);
    }

    public function store(Request $request, CreateResourceItem $createAction) {
        if ($request->user()->cannot('create', Item::class)) {
            // return Redirect::back()->withErrors(['error' => 'Not authorized']);
            return response()->json(['message' => 'Not authorized'], 403);
        }

        $request->validate([
            'subtype' => 'required',
            // 'title' => 'string',
            'url' => 'url|required_if:subtype,link',
            'files' => 'required_if:subtype,image,image.portrait,image.square,image.icon',
            'files.*' => 'file'
        ]);

        return response()->json($createAction->execute($request));
    }    

    public function uploadFile(Request $request) {
        if ($request->user()->cannot('create', Item::class)) {
            // return Redirect::back()->withErrors(['error' => 'Not authorized']);
            return response()->json(['message' => 'Not authorized'], 403);
        }
        $request->validate([
            'content_id' => 'required',            
            'files' => 'required',
            'files.*' => 'file'
        ]);

        $content = ItemContent::withoutGlobalScopes()
                        ->find($request->input('content_id'));
        
        foreach ($request->file('files') as $file) {
            $path = $file->store('files');
            $path = Str::replaceFirst('files/', '', $path);
            $content->files()->create([
                'path' => $path,
                'original_filename' => $file->getClientOriginalName(),
                'label' => $request->input('label')
            ]);
        }

        return response()->json(['success' => true, 'path' => $path]);
        // return Redirect::route('admin.items.edit', $content->item_id)
        //             ->with(['info' => 'File uploaded']);
    }

    public function replaceImage(Request $request) {
        if ($request->user()->cannot('create', Item::class)) {
            // return Redirect::back()->withErrors(['error' => 'Not authorized']);
            return response()->json(['message' => 'Not authorized'], 403);
        }
        $request->validate([
            'content_id' => 'integer|required',            
            'file' => 'required|file',
        ]);

        $content = ItemContent::withoutGlobalScopes()
                        ->find($request->input('content_id'));
        
        $file = $request->file('file');
        $path = $file->store('img');
        $path = Str::replaceFirst('img/', '', $path);

        //Azure Code Added By Cyblance start
        $file1 = $request->file('file');
        $path1 = $file1->getRealPath();
        $type = $file1->getMimeType();

        self::storageAddFile("blockblobszjdmoh", $path1, $path, $type);
         
        $content->update([
            'azure'=>'Yes'
        ]);
        //Azure Code Added By Cyblance end
        $content->images()->updateOrCreate([
            'content_id' => $content->id
        ], [
            'path' => $path,
        ]);

        return response()->json(['success' => true, 'path' => $path]);
    }

    //Azure Code Added By Cyblance start
    public function storageAddFile($containerName, $file, $fileName, $type){
        $connectionString = "DefaultEndpointsProtocol=https;AccountName=eiwebsite;AccountKey=pAFH72YKlkWzJAL1rhV1bDIt9u6DUw7vmrzmW17uI9eE6caljMirl4TkE/iPUrKv2MMIN+SFA1vZ3ubCgXy2ow==";
        $blobClient = BlobRestProxy::createBlobService($connectionString);
        $handle = @fopen($file, "r");
        if($handle){
            $options = new CreateBlockBlobOptions();
            $mime = NULL;
            try{
                $options->setContentType($type);
            }catch ( Exception $e ){
                error_log("Failed to read mime from '".$file.": ". $e);
            } 
            try{
                if($mime){  
                    $options->setCacheControl("public, max-age=604800");
                }
                $blobClient->createBlockBlob($containerName, $fileName, $handle, $options);
            } catch ( Exception $e ) {
                error_log("Failed to upload file '".$file."' to storage: ". $e);
            } 
           
            return true;
        } else {        
            error_log("Failed to open file '".$file."' to upload to storage.");
            return false;
        }
        @fclose($handle);
    }

    public function storageRemoveFile($containerName, $fileName){

        $connectionString = "DefaultEndpointsProtocol=https;AccountName=eiwebsite;AccountKey=pAFH72YKlkWzJAL1rhV1bDIt9u6DUw7vmrzmW17uI9eE6caljMirl4TkE/iPUrKv2MMIN+SFA1vZ3ubCgXy2ow==";
        $blobClient = BlobRestProxy::createBlobService($connectionString);
        try{
            $blobClient->deleteBlob($containerName, $fileName);
        } catch ( Exception $e ) {
            error_log("Failed to delete file '".$fileName."' from storage");
        } 
        return true;
    }
    //Azure Code Added By Cyblance end

}