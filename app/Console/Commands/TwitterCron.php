<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Response,Session,Twitter,File;
use DateTime, DateTimeZone;
use App\Models\ItemContent;
use App\Models\CollectionContent;
use Illuminate\Support\Str;

class TwitterCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publishtwittercron:twitterschedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $current_datetime = date('Y-m-d H:i');
        $currentDate = $current_datetime.':00';
        $item = ItemContent::where('twitter_shechdule_date','=',$currentDate)->get();
        $collection = CollectionContent::where('twitter_shechdule_date','=',$currentDate)->get();
        if($item){
            //echo $current_datetime; exit();
            foreach ($item as $key => $value) {
               $title=$value->title;
                $subtitle=$value->subtitle;
                $slug=$value->slug;
                $itemUrl=$value->lang.'/item/'.$value->item_id.':'.$slug;
                $url=getCurrentHost().'/'.$itemUrl;
                if($subtitle==''){
                    $subtitle=$title;
                }else{
                     $subtitle=$subtitle;
                }
                $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";

                $curl = curl_init();
                $headers = array("Content-type: text/xml");
                curl_setopt_array($curl, array(
                CURLOPT_URL => $simsrc,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_FOLLOWLOCATION =>1,
                ));
                $results = curl_exec($curl);
                if($currentDate==$value->twitter_shechdule_date){
                    $postText = strip_tags(Str::limit($title,50));
                    $newTwitte = ['status' => $postText.' '.$url];
                    $uploaded_media = Twitter::uploadMedia(['media' => $results]);
                    $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                    $configTwitter= Twitter::postTweet($newTwitte); 
                    Twitter::linkify($configTwitter);
                }
            }
            echo 'Twitter Cron job run successfully at. '.$currentDate; 
        }elseif($collection){  
            foreach ($collection as $key => $value) {
                $title=$value->title;
                $subtitle=$value->subtitle;
                $slug=$value->slug;
                $collectionUrl=$value->lang.'/collection/'.$value->collection_id.':'.$slug;
                $url=getCurrentHost().'/'.$collectionUrl;
                if($subtitle==''){
                $subtitle=$title;
                }else{
                $subtitle=$subtitle;
                }
                $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";

                $curl = curl_init();
                $headers = array("Content-type: text/xml");
                curl_setopt_array($curl, array(
                CURLOPT_URL => $simsrc,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_FOLLOWLOCATION =>1,
                ));
                $results = curl_exec($curl);
                if($currentDate==$value->twitter_shechdule_date){
                    $postText = strip_tags(Str::limit($title,50));
                    $newTwitte = ['status' => $postText.' '.$url];
                    $uploaded_media = Twitter::uploadMedia(['media' => $results]);
                    $newTwitte['media_ids'][$uploaded_media->media_id_string] = $uploaded_media->media_id_string;
                    $configTwitter= Twitter::postTweet($newTwitte); 
                    Twitter::linkify($configTwitter);
                }
            }
         echo 'Twitter Cron job run successfully at. '.$currentDate; 
        }
    }
}
