<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Facebook;
use App\Models\ItemContent;
use App\Models\CollectionContent;
use App\Classes\FacebookSdk\MyLaravelPersistentDataHandler;

class ScheduleFacebookShare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook:ScheduledShare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Post on facebook page on scheduled time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		
		$current_datetime = date('Y-m-d H:i');
        $currentDate = $current_datetime.':00';
        $item = ItemContent::where('facebook_shechdule_date','=',$currentDate)->get();
		$collection = CollectionContent::where('twitter_shechdule_date','=',$currentDate)->get();
		
		if($item){
			//echo "item"; exit();
			$fbtoken=get_social_access_token('facebook');
			$fb = new Facebook\Facebook([
				'app_id'=> '2797063680569641',
				'app_secret'=> 'f5f0ff23cb1492ecf59e6eb31ed5ea5e',
				'default_graph_version' => 'v2.10',
				'persistent_data_handler' => new MyLaravelPersistentDataHandler(),
			]);
			//echo "<pre>"; print_r($fb); exit();
			$oAuth2Client = $fb->getOAuth2Client();
			$tokenMetadata = $oAuth2Client->debugToken($fbtoken->token);
			
			if($tokenMetadata->isError()){
				if($tokenMetadata->getErrorCode() == 190){
					$this->info( $info . 'User Access Token is Expired');
					\Log::info($info.'User Access Token is Expired');
					return;
				}else{
					$this->info($info.$tokenMetadata->getErrorMessage());
					\Log::info( $info. $tokenMetadata->getErrorMessage());
					return;
				}
			}else{
				if($fbtoken->token){
					$fb = new Facebook\Facebook([
						'app_id'=> '2797063680569641',
						'app_secret'=> 'f5f0ff23cb1492ecf59e6eb31ed5ea5e',
						'default_graph_version' => 'v2.10',
						'persistent_data_handler' => new MyLaravelPersistentDataHandler(),
						]); 
					try {
						$response = $fb->get(
							'/debug_token?input_token='.$fbtoken->token,$fbtoken->user_token
						);
					} catch(Facebook\Exceptions\FacebookResponseException $e) {
						$this->info($info.'Graph returned an error: ' . $e->getMessage() .'- Cron Job Fail');
						return;
					} catch(Facebook\Exceptions\FacebookSDKException $e) {
						$this->info($info.'Facebook SDK returned an error: ' . $e->getMessage().'- Cron Job Fail');
						return;
					}
					$graphNode = $response->getGraphNode();
					if($graphNode->getField('is_valid')){
						foreach($item as $value){

							$title=$value->title;
			                $subtitle=$value->subtitle;
			                $slug=$value->slug;
			                $itemUrl=$value->lang.'/item/'.$value->item_id.':'.$slug;
			                $url=getCurrentHost().'/'.$itemUrl;
			                if($subtitle==''){
			                    $subtitle=$title;
			                }else{
			                     $subtitle=$subtitle;
			                }
			                    $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";
							try {
								$response = $fb->post(
									'/me/feed',
									array (
										'message' => $title,
										'link' => $url,
										'published' => '1'
									),
									$fbtoken->token
								);
							} catch(FacebookExceptionsFacebookResponseException $e) {
								$this->info($info. $titile .' - Cron Job Fail');
							} catch(FacebookExceptionsFacebookSDKException $e) {
								$this->info($info. $titile .' - Cron Job Fail');
							}

							$graphNode = $response->getGraphNode();
							$this->info($info. $titile .'Posted with id: ' . $graphNode['id']);
						}
						 echo 'Facebook Cron job run successfully at. '.$currentDate; 
					}else{
						$this->info($info. 'page access token is invalid - Cron Job Fail');
						return;
					}
				}
			}
		}elseif($collection){
			//echo "collection"; exit();
			$fbtoken=get_social_access_token('facebook');
			$fb = new Facebook\Facebook([
				'app_id'=> '2797063680569641',
				'app_secret'=> 'f5f0ff23cb1492ecf59e6eb31ed5ea5e',
				'default_graph_version' => 'v2.10',
				'persistent_data_handler' => new MyLaravelPersistentDataHandler(),
			]);
			$oAuth2Client = $fb->getOAuth2Client();
			$tokenMetadata = $oAuth2Client->debugToken($fbtoken->token);
			if($tokenMetadata->isError()){
				if($tokenMetadata->getErrorCode() == 190){
					$this->info( $info . 'User Access Token is Expired');
					\Log::info($info.'User Access Token is Expired');
					return;
				}else{
					$this->info($info.$tokenMetadata->getErrorMessage());
					\Log::info( $info. $tokenMetadata->getErrorMessage());
					return;
				}
			}else{
				if($fbtoken->token){
					$fb = new Facebook\Facebook([
						'app_id'=> '2797063680569641',
						'app_secret'=> 'f5f0ff23cb1492ecf59e6eb31ed5ea5e',
						'default_graph_version' => 'v2.10',
						'persistent_data_handler' => new MyLaravelPersistentDataHandler(),
						]); 
					try {
						$response = $fb->get(
							'/debug_token?input_token='.$fbtoken->token,$fbtoken->user_token
						);
					} catch(Facebook\Exceptions\FacebookResponseException $e) {
						$this->info($info.'Graph returned an error: ' . $e->getMessage() .'- Cron Job Fail');
						return;
					} catch(Facebook\Exceptions\FacebookSDKException $e) {
						$this->info($info.'Facebook SDK returned an error: ' . $e->getMessage().'- Cron Job Fail');
						return;
					}
					$graphNode = $response->getGraphNode();
					if($graphNode->getField('is_valid')){
						foreach($collection as $value){

							$title=$value->title;
			                $subtitle=$value->subtitle;
			                $slug=$value->slug;
			                $collectionUrl=$value->lang.'/collection/'.$value->collection_id.':'.$slug;
			                $url=getCurrentHost().'/'.$collectionUrl;
			                if($subtitle==''){
			                    $subtitle=$title;
			                }else{
			                     $subtitle=$subtitle;
			                }
			                    $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";
							try {
								$response = $fb->post(
									'/me/feed',
									array (
										'message' => $title,
										'link' => $url,
										'published' => '1'
									),
									$fbtoken->token
								);
							} catch(FacebookExceptionsFacebookResponseException $e) {
								$this->info($info. $titile .' - Cron Job Fail');
							} catch(FacebookExceptionsFacebookSDKException $e) {
								$this->info($info. $titile .' - Cron Job Fail');
							}

							$graphNode = $response->getGraphNode();
							$this->info($info. $titile .'Posted with id: ' . $graphNode['id']);
						}
						 echo 'Facebook Cron job run successfully at. '.$currentDate; 
					}else{
						$this->info($info. 'page access token is invalid - Cron Job Fail');
						return;
					}
				}
			}
		}
    }
}
