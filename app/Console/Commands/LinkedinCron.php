<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Validator;
use App\Models\ItemContent;
use GuzzleHttp\Client;
use App\Models\CollectionContent;

class LinkedinCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'linkedin:linkedinschedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $current_datetime = date('Y-m-d H:i');
        $currentDate = $current_datetime.':00';
        $item = ItemContent::where('linkeden_sechulde_date','=',$currentDate)->get();
        $collection = CollectionContent::where('linkeden_sechulde_date','=',$currentDate)->get();
        if($item){
            foreach ($item as $key => $value) {
                $title=$value->title;
                $subtitle=$value->subtitle;
                $slug=$value->slug;
                $itemUrl=$value->lang.'/item/'.$value->item_id.':'.$slug;
                $url=getCurrentHost().'/'.$itemUrl;
                if($subtitle==''){
                    $subtitle=$title;
                }else{
                     $subtitle=$subtitle;
                }
                    $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";
                
                $access_token = linkedin_token();
                try {
                    $client = new Client(['base_uri' => 'https://api.linkedin.com']);
                    $response = $client->request('GET', '/v2/me', [
                        'headers' => [
                            "Authorization" => "Bearer " . $access_token,
                        ],
                    ]);
                    $data = json_decode($response->getBody()->getContents(), true);
                    $linkedin_profile_id = $data['id']; // store this id somewhere
                } catch(Exception $e) {
                    echo $e->getMessage();
                }
                $link = $url;
                $access_token = $access_token;
                $linkedin_id = $linkedin_profile_id;
                $body = new \stdClass();
                $body->content = new \stdClass();
                $body->content->contentEntities[0] = new \stdClass();
                $body->text = new \stdClass();
                $body->content->contentEntities[0]->thumbnails[0] = new \stdClass();
                $body->content->contentEntities[0]->entityLocation = $link;
                $body->content->contentEntities[0]->thumbnails[0]->resolvedUrl = $simsrc;
                $body->content->title = $title;
                $body->owner = 'urn:li:person:'.$linkedin_id;
                $body->text->text = $subtitle;
                $body_json = json_encode($body, true);
                if($currentDate==$value->linkeden_sechulde_date){

                    try {
                        $client = new Client(['base_uri' => 'https://api.linkedin.com']);
                        $response = $client->request('POST', '/v2/shares', [
                            'headers' => [
                                "Authorization" => "Bearer " . $access_token,
                                "Content-Type"  => "application/json",
                                "x-li-format"   => "json"
                            ],
                            'body' => $body_json,
                        ]);
                     
                        if ($response->getStatusCode() !== 201) {
                            echo 'Error: '. $response->getLastBody()->errors[0]->message;
                        }
                        if ($response->getStatusCode() == 422) {
                            echo 'Error: '. $response->getLastBody()->errors[0]->message; exit();
                        }
                    } catch(Exception $e) {
                        echo $e->getMessage(). ' for link '. $link;
                    }
                }
            }
            echo 'Cron job run successfully at. '.$currentDate; 
        }elseif($collection){
            foreach ($collection as $key => $value) {
                $title=$value->title;
                $subtitle=$value->subtitle;
                $slug=$value->slug;
                $collectionUrl=$value->lang.'/collection/'.$value->collection_id.':'.$slug;
                $url=getCurrentHost().'/'.$collectionUrl;
                if($subtitle==''){
                    $subtitle=$title;
                }else{
                     $subtitle=$subtitle;
                }
                    $simsrc= "https://www.ei-ie.org/img/placeholder.jpg";
                
                $access_token = linkedin_token();
                try {
                    $client = new Client(['base_uri' => 'https://api.linkedin.com']);
                    $response = $client->request('GET', '/v2/me', [
                        'headers' => [
                            "Authorization" => "Bearer " . $access_token,
                        ],
                    ]);
                    $data = json_decode($response->getBody()->getContents(), true);
                    $linkedin_profile_id = $data['id']; // store this id somewhere
                } catch(Exception $e) {
                    echo $e->getMessage();
                }
                $link = $url;
                $access_token = $access_token;
                $linkedin_id = $linkedin_profile_id;
                $body = new \stdClass();
                $body->content = new \stdClass();
                $body->content->contentEntities[0] = new \stdClass();
                $body->text = new \stdClass();
                $body->content->contentEntities[0]->thumbnails[0] = new \stdClass();
                $body->content->contentEntities[0]->entityLocation = $link;
                $body->content->contentEntities[0]->thumbnails[0]->resolvedUrl = $simsrc;
                $body->content->title = $title;
                $body->owner = 'urn:li:person:'.$linkedin_id;
                $body->text->text = $subtitle;
                $body_json = json_encode($body, true);
                if($currentDate==$value->linkeden_sechulde_date){

                    try {
                        $client = new Client(['base_uri' => 'https://api.linkedin.com']);
                        $response = $client->request('POST', '/v2/shares', [
                            'headers' => [
                                "Authorization" => "Bearer " . $access_token,
                                "Content-Type"  => "application/json",
                                "x-li-format"   => "json"
                            ],
                            'body' => $body_json,
                        ]);
                     
                        if ($response->getStatusCode() !== 201) {
                            echo 'Error: '. $response->getLastBody()->errors[0]->message;
                        }
                        if ($response->getStatusCode() == 422) {
                            echo 'Error: '. $response->getLastBody()->errors[0]->message; exit();
                        }
                    } catch(Exception $e) {
                        echo $e->getMessage(). ' for link '. $link;
                    }
                }
            }
            echo 'Cron job run successfully at. '.$currentDate; 
        }
    }
}
